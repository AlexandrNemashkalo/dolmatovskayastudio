package ru.dolmatovskaya.studio.contacts.api.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class ContactDto(
    @field:NotBlank
    @field:Size(max = 32)
    val contactType: String,
    @field:NotBlank
    @field:Size(max = 64)
    val nameRus: String,
    @field:NotBlank
    @field:Size(max = 64)
    val nameEng: String,
    @field:Size(max = 512)
    val url: String? = null,
    val id: Int? = null,
) {
    fun withId(id: Int) = ContactDto(contactType, nameRus, nameEng, url, id)
}
