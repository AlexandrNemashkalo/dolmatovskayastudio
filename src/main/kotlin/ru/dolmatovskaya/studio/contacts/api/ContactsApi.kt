package ru.dolmatovskaya.studio.contacts.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.validation.Valid
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive
import org.springframework.validation.annotation.Validated
import ru.dolmatovskaya.studio.contacts.api.dto.ContactDto
import ru.dolmatovskaya.studio.contacts.api.dto.ContactListResponseDto
import ru.dolmatovskaya.studio.users.model.User

@Validated
interface ContactsApi {
    @Operation(
        description = "Получить список контактов",
        summary = "Для неавторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun get(): ContactListResponseDto

    @Operation(
        description = "Получить контакт по id",
        summary = "Для неавторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun get(@Positive @Parameter(`in` = ParameterIn.PATH) id: Int): ContactDto

    @Operation(
        description = "Обновить запись о контакте",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "ОК", responseCode = "200")]
    )
    fun update(@Positive @Parameter(`in` = ParameterIn.PATH) id: Int, @Valid @RequestBody dto: ContactDto, @Parameter(hidden = true) user: User)

    @Operation(
        description = "Создать запись о контакте",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "ОК", responseCode = "200")]
    )
    fun create(dto: ContactDto, @Parameter(hidden = true) user: User)

    @Operation(
        description = "Удалить запись о контакте",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "ОК", responseCode = "200")]
    )
    fun delete(@Positive @Parameter(`in` = ParameterIn.PATH) id: Int, @Parameter(hidden = true) user: User)

    @Operation(
        description = "Удалить запись о контактах",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "ОК", responseCode = "200")]
    )
    fun deleteBatch(@NotEmpty @Parameter(`in` = ParameterIn.QUERY) ids: Set<@Positive Int>, @Parameter(hidden = true) user: User)
}