package ru.dolmatovskaya.studio.contacts.api.dto

data class ContactListResponseDto(
    val totalCount: Long,
    val value: List<ContactDto> = emptyList()
)