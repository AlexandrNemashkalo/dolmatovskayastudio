package ru.dolmatovskaya.studio.contacts.resource

import mu.KotlinLogging
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.CONTACTS
import ru.dolmatovskaya.studio.contacts.api.ContactsApi
import ru.dolmatovskaya.studio.contacts.api.dto.ContactDto
import ru.dolmatovskaya.studio.contacts.api.dto.ContactListResponseDto
import ru.dolmatovskaya.studio.contacts.service.ContactService
import ru.dolmatovskaya.studio.users.model.User

private val log = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$CONTACTS")
@CrossOrigin("*")
class ContactsApiImpl(
    private val contactService: ContactService
) : ContactsApi {

    @PostMapping
    override fun create(@RequestBody dto: ContactDto, @AuthenticationPrincipal user: User) {
        log.info { "Create by $user $dto" }
        contactService.create(dto)
    }

    @GetMapping("{id}")
    override fun get(@PathVariable id: Int): ContactDto {
        return contactService.get(id)
    }

    @GetMapping
    override fun get(): ContactListResponseDto {
        return contactService.get()
    }

    @PutMapping("{id}")
    override fun update(@PathVariable id: Int, @RequestBody dto: ContactDto, @AuthenticationPrincipal user: User) {
        log.info { "Update by $user $id $dto" }
        contactService.update(dto.withId(id))
    }

    @DeleteMapping("{id}")
    override fun delete(@PathVariable id: Int, @AuthenticationPrincipal user: User) {
        log.info { "Delete by $user $id" }
        contactService.delete(id)
    }

    @DeleteMapping
    override fun deleteBatch(@RequestParam ids: Set<Int>, @AuthenticationPrincipal user: User) {
        log.info { "Delete by $user $ids" }
        contactService.deleteBatch(ids)
    }
}