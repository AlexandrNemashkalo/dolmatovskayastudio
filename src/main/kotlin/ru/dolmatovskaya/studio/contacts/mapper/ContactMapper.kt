package ru.dolmatovskaya.studio.contacts.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.contacts.api.dto.ContactDto
import ru.dolmatovskaya.studio.contacts.model.Contact

@Mapper
interface ContactMapper {
    fun fromDto(dto: ContactDto): Contact
    fun fromEntity(entity: Contact): ContactDto
    fun fromEntities(entities: Collection<Contact>): List<ContactDto>
}