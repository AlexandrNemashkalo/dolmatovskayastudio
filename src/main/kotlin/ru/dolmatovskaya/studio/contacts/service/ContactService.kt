package ru.dolmatovskaya.studio.contacts.service

import ru.dolmatovskaya.studio.contacts.api.dto.ContactDto
import ru.dolmatovskaya.studio.contacts.api.dto.ContactListResponseDto

interface ContactService {
    fun get(): ContactListResponseDto
    fun get(id: Int): ContactDto
    fun update(dto: ContactDto)
    fun create(dto: ContactDto)
    fun delete(id: Int)
    fun deleteBatch(ids: Collection<Int>)
}