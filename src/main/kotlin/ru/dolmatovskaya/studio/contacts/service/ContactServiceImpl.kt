package ru.dolmatovskaya.studio.contacts.service

import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import ru.dolmatovskaya.studio.contacts.api.dto.ContactDto
import ru.dolmatovskaya.studio.contacts.api.dto.ContactListResponseDto
import ru.dolmatovskaya.studio.contacts.mapper.ContactMapper
import ru.dolmatovskaya.studio.contacts.repository.ContactRepository
import ru.dolmatovskaya.studio.extensions.orElseThrowNotFound

@Service
class ContactServiceImpl(
    private val contactRepository: ContactRepository,
    private val contactMapper: ContactMapper
) : ContactService {

    override fun create(dto: ContactDto) {
        contactRepository.save(contactMapper.fromDto(dto))
    }

    override fun get(id: Int): ContactDto {
        return contactRepository.findById(id)
            .map { contactMapper.fromEntity(it) }
            .orElseThrowNotFound("Contact $id not found")
    }

    override fun get(): ContactListResponseDto {
        val result = contactMapper.fromEntities(contactRepository.findAll(Sort.by(Sort.Order.asc("id"))))
        return ContactListResponseDto(result.size.toLong(), result)
    }

    override fun update(dto: ContactDto) {
        contactRepository.save(contactMapper.fromDto(dto))
    }

    override fun delete(id: Int) {
        contactRepository.deleteById(id)
    }

    override fun deleteBatch(ids: Collection<Int>) {
        contactRepository.deleteAllByIdInBatch(ids)
    }
}