package ru.dolmatovskaya.studio.contacts.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.dolmatovskaya.studio.contacts.model.Contact

interface ContactRepository : JpaRepository<Contact, Int>