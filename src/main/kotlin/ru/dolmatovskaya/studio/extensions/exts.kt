package ru.dolmatovskaya.studio.extensions

import java.util.Optional
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

private val logger = KotlinLogging.logger { }

fun <T> Optional<T>.orElseThrowNotFound(message: String): T = orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND, message).also {
    logger.warn { it.message }
} }
fun Exception.andLog() = this.also { logger.warn { it.message } }

fun String.normalizePhone() = this.replace(Regex("\\+|-|\\(|\\)|\\s|[a-zA-Z]|,|\\.|~|;|:"), "")