package ru.dolmatovskaya.studio.pictures.db.repository

import java.util.UUID
import org.springframework.data.jpa.repository.JpaRepository
import ru.dolmatovskaya.studio.pictures.db.model.Picture

interface PictureRepository : JpaRepository<Picture, UUID> {
    fun deleteAllByDomainIdInAndDomainAddInfo(ids: Collection<Long>, domainAddInfo: String)
    fun findAllByDomainIdAndDomainAddInfo(domainId: Long, domainAddInfo: String): List<Picture>
    fun findAllByDomainIdInAndDomainAddInfo(domainIds: Collection<Long>, domainAddInfo: String): List<Picture>
}