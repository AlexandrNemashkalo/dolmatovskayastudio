package ru.dolmatovskaya.studio.pictures.db.model

import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "pictures")
class Picture(
    val domainId: Long,
    val domainAddInfo: String,
    @Column(name = "s3_id")
    val s3Id: String,
    @Id
    val id: UUID = UUID.randomUUID(),
)