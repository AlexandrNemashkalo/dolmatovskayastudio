package ru.dolmatovskaya.studio.pictures.db.service

import java.util.UUID
import mu.KotlinLogging
import org.springframework.stereotype.Service
import ru.dolmatovskaya.studio.extensions.orElseThrowNotFound
import ru.dolmatovskaya.studio.pictures.db.model.Picture
import ru.dolmatovskaya.studio.pictures.db.repository.PictureRepository
import ru.dolmatovskaya.studio.pictures.domain.GetByIdAndDomainAddInfoRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainCreateRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.pictures.domain.GetByPictureUUIDRequest
import ru.dolmatovskaya.studio.pictures.mapper.PictureMapper

private val logger = KotlinLogging.logger { }

@Service
class PictureDBServiceImpl(
    private val pictureRepository: PictureRepository,
    private val pictureMapper: PictureMapper
) : PictureDBService {

    override fun save(pictures: List<PictureDomainCreateRequest>) {
        pictureRepository.saveAll(pictureMapper.toEntities(pictures))
    }

    override fun get(picture: GetByPictureUUIDRequest): Picture {
        return pictureRepository.findById(picture.id).orElseThrowNotFound("picture not found ${picture.id}")
    }

    override fun getByIdAndDomain(req: GetByIdAndDomainAddInfoRequest): List<Picture> {
        return pictureRepository.findAllByDomainIdAndDomainAddInfo(req.id, req.domainAddInfo)
    }

    override fun getImageUUIDs(picture: PictureDomainGetLightRequest): List<UUID> {
        return pictureRepository.findAllByDomainIdAndDomainAddInfo(picture.domainId, picture.domainAddInfo).map { it.id }
    }

    override fun getS3PathByUUIDs(uuids: Collection<UUID>): List<String> {
        return pictureRepository.findAllById(uuids).map { it.s3Id }
    }

    override fun getImageS3Paths(picture: PictureDomainGetLightRequest): List<String> {
        val pictures = pictureRepository.findAllByDomainIdAndDomainAddInfo(picture.domainId, picture.domainAddInfo)
        logger.debug { "pictures from db = $pictures" }
        return pictures.map { it.s3Id }
    }

    override fun deleteByIds(uuids: Collection<UUID>) {
        pictureRepository.deleteAllByIdInBatch(uuids)
    }

    override fun delete(picture: PictureDomainDeleteRequest) {
        val map = picture.domainId.asSequence().partition { it.value == null }
        pictureRepository.deleteAllByDomainIdInAndDomainAddInfo(map.first.map { it.key }, picture.domainAddInfo)
        pictureRepository.deleteAllByIdInBatch(map.second.flatMap { it.value!! })
    }

    override fun getAllByIds(pictures: List<PictureDomainGetLightRequest>): List<Picture> {
        return pictureRepository.findAllByDomainIdInAndDomainAddInfo(pictures.map { it.domainId }, pictures.first().domainAddInfo)
    }
}