package ru.dolmatovskaya.studio.pictures.db.service

import java.util.UUID
import ru.dolmatovskaya.studio.pictures.db.model.Picture
import ru.dolmatovskaya.studio.pictures.domain.GetByIdAndDomainAddInfoRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainCreateRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.pictures.domain.GetByPictureUUIDRequest

interface PictureDBService {
    fun save(pictures: List<PictureDomainCreateRequest>)
    fun get(picture: GetByPictureUUIDRequest): Picture
    fun getByIdAndDomain(req: GetByIdAndDomainAddInfoRequest): List<Picture>
    fun getAllByIds(pictures: List<PictureDomainGetLightRequest>): List<Picture>
    fun delete(picture: PictureDomainDeleteRequest)
    fun deleteByIds(uuids: Collection<UUID>)
    fun getImageUUIDs(picture: PictureDomainGetLightRequest): List<UUID>
    fun getS3PathByUUIDs(uuids: Collection<UUID>): List<String>
    fun getImageS3Paths(picture: PictureDomainGetLightRequest): List<String>
}