package ru.dolmatovskaya.studio.pictures.api

import java.util.UUID
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainCreateRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.pictures.domain.GetByPictureUUIDRequest
import ru.dolmatovskaya.studio.pictures.domain.UpdatePictureRequest

interface PictureService {
    fun create(picture: PictureDomainCreateRequest): List<PictureDomainCreateRequest>
    fun delete(picture: PictureDomainDeleteRequest, deleteFolderItself: Boolean = true)
    fun get(picture: GetByPictureUUIDRequest): Pair<Resource, MediaType>
    fun getLight(picture: PictureDomainGetLightRequest): List<UUID>
    fun getLights(pictures: List<PictureDomainGetLightRequest>): Map<Long, List<UUID>>
    fun update(req: UpdatePictureRequest)
}