package ru.dolmatovskaya.studio.pictures.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import java.util.UUID
import javax.validation.constraints.NotBlank
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated

@Validated
interface PictureApi {

    @Operation(
        description = "Получить картинку по ее uuid",
        summary = "Для неавторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200", content = [Content(mediaType = MediaType.IMAGE_GIF_VALUE)]),
            ApiResponse(description = "OK", responseCode = "200", content = [Content(mediaType = MediaType.IMAGE_PNG_VALUE)]),
            ApiResponse(description = "OK", responseCode = "200", content = [Content(mediaType = MediaType.IMAGE_JPEG_VALUE)]),
            ApiResponse(description = "NOT_FOUND", responseCode = "404", content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE)]),
        ]
    )
    fun get(
        @NotBlank
        @Parameter(`in` = ParameterIn.PATH) domainInfo: String,
        @Parameter(`in` = ParameterIn.PATH) id: UUID
    ): ResponseEntity<Resource>

}