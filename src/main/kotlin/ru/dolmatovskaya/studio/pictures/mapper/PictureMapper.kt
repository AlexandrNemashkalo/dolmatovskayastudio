package ru.dolmatovskaya.studio.pictures.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.pictures.db.model.Picture
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainCreateRequest

@Mapper
interface PictureMapper {
    fun toEntity(domain: PictureDomainCreateRequest): Picture = Picture(domain.domainId, domain.domainAddInfo, domain.s3Id)
    fun toEntities(domains: Collection<PictureDomainCreateRequest>): List<Picture>
}