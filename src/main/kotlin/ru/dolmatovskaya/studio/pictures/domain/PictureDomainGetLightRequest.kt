package ru.dolmatovskaya.studio.pictures.domain

import ru.dolmatovskaya.studio.consts.Domains

data class PictureDomainGetLightRequest(
    val domainId: Long,
    val domainAddInfo: String,
) {
    companion object {
        fun film(id: Long) = PictureDomainGetLightRequest(id, Domains.FILMS)
        fun collaboration(id: Long) = PictureDomainGetLightRequest(id, Domains.COLLABORATIONS)
        fun commercial(id: Long) = PictureDomainGetLightRequest(id, Domains.COMMERCIALS)
        fun media(id: Long) = PictureDomainGetLightRequest(id, Domains.MEDIA)
        fun musicVideo(id: Long) = PictureDomainGetLightRequest(id, Domains.MUSIC_VIDEOS)
        fun theater(id: Long) = PictureDomainGetLightRequest(id, Domains.THEATERS)
    }
}