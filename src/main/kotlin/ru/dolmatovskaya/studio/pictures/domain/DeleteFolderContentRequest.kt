package ru.dolmatovskaya.studio.pictures.domain

data class DeleteFolderContentRequest(
    val folderName: String,
    val ids: Collection<String>,
)