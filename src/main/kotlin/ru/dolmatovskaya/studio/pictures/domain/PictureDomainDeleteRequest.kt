package ru.dolmatovskaya.studio.pictures.domain

import java.util.UUID
import ru.dolmatovskaya.studio.consts.Domains

data class PictureDomainDeleteRequest(
    val domainId: Map<Long, Collection<UUID>?>,
    val domainAddInfo: String,
) {
    companion object {
        fun film(ids: Collection<Long>) = PictureDomainDeleteRequest(ids.associateWith { null }, Domains.FILMS)
        fun collaboration(pairs: Map<Long, Collection<UUID>?>) = PictureDomainDeleteRequest(pairs, Domains.COLLABORATIONS)
        fun commercial(ids: Collection<Long>) = PictureDomainDeleteRequest(ids.associateWith { null }, Domains.COMMERCIALS)
        fun media(ids: Collection<Long>) = PictureDomainDeleteRequest(ids.associateWith { null }, Domains.MEDIA)
        fun musicVideo(ids: Collection<Long>) = PictureDomainDeleteRequest(ids.associateWith { null }, Domains.MUSIC_VIDEOS)
        fun theater(pairs: Map<Long, Collection<UUID>?>) = PictureDomainDeleteRequest(pairs, Domains.THEATERS)
    }
}