package ru.dolmatovskaya.studio.pictures.domain

import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.consts.Domains

data class UpdatePictureRequest(
    val domainId: Long,
    val domainAddInfo: String,
    val file: MultipartFile
) {
    companion object {
        fun film(domainId: Long, file: MultipartFile) = UpdatePictureRequest(domainId, Domains.FILMS, file)
        fun commercial(domainId: Long, file: MultipartFile) = UpdatePictureRequest(domainId, Domains.COMMERCIALS, file)
        fun media(domainId: Long, file: MultipartFile) = UpdatePictureRequest(domainId, Domains.MEDIA, file)
        fun musicVideo(domainId: Long, file: MultipartFile) = UpdatePictureRequest(domainId, Domains.MUSIC_VIDEOS, file)
    }
}