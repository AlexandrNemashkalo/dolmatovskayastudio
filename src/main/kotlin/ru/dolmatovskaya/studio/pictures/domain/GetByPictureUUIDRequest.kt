package ru.dolmatovskaya.studio.pictures.domain

import java.util.UUID

data class GetByPictureUUIDRequest(
    val id: UUID,
)