package ru.dolmatovskaya.studio.pictures.domain

import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.consts.Domains

data class PictureDomainCreateRequest(
    val domainId: Long,
    val domainAddInfo: String,
    val files: Collection<MultipartFile>,
) {
    lateinit var s3Id: String

    fun withS3Id(s3Id: String) = PictureDomainCreateRequest(domainId, domainAddInfo, files).apply { this.s3Id = s3Id }

    companion object {
        fun commercial(domainId: Long, files: Collection<MultipartFile>) = PictureDomainCreateRequest(domainId, Domains.COMMERCIALS, files)
        fun film(domainId: Long, files: Collection<MultipartFile>) = PictureDomainCreateRequest(domainId, Domains.FILMS, files)
        fun collaboration(domainId: Long, files: Collection<MultipartFile>) = PictureDomainCreateRequest(domainId, Domains.COLLABORATIONS, files)
        fun theater(domainId: Long, files: Collection<MultipartFile>) = PictureDomainCreateRequest(domainId, Domains.THEATERS, files)
    }
}

