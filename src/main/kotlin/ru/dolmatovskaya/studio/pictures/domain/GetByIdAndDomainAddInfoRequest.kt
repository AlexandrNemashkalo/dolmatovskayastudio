package ru.dolmatovskaya.studio.pictures.domain

import ru.dolmatovskaya.studio.consts.Domains

data class GetByIdAndDomainAddInfoRequest(
    val id: Long,
    val domainAddInfo: String
) {
    companion object {
        fun film(id: Long) = GetByIdAndDomainAddInfoRequest(id, Domains.FILMS)
    }
}
