package ru.dolmatovskaya.studio.pictures.s3.service

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import java.time.Duration
import java.util.UUID
import mu.KotlinLogging
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.WebClientResponseException
import org.springframework.web.reactive.function.client.bodyToMono
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.exceptions.S3Exception
import ru.dolmatovskaya.studio.pictures.domain.DeleteFolderContentRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainCreateRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.property.S3Properties

private val logger = KotlinLogging.logger { }

@Service
class S3ServiceImpl(
    private val calculationService: CalculationService,
    private val s3WebClient: WebClient,
    private val s3Properties: S3Properties
) : S3Service {

    override fun save(domain: PictureDomainCreateRequest): List<PictureDomainCreateRequest> {
        return try {
            domain.files.map {
                val rawFileBytes = it.bytes
                val fileExtension = (it.originalFilename ?: "").substringAfterLast(".")
                val dbPath = "${s3Properties.domainMap[domain.domainAddInfo]!!}/${domain.domainId}/${UUID.randomUUID()}.$fileExtension"
                val path = "/${s3Properties.bucketName}/$dbPath"
                val hash = calculationService.calculateSha256Hash(rawFileBytes)
                val date = calculationService.calculateDate()
                s3WebClient.put()
                    .uri { it.path(path).build() }
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE)
                    .header("x-amz-date", date)
                    .header("x-amz-content-sha256", hash)
                    .header(HttpHeaders.AUTHORIZATION, calculationService.calculateAuthorizationHeader(HttpMethod.PUT, date, path, hash))
                    .bodyValue(rawFileBytes)
                    .retrieve()
                    .toBodilessEntity()
                    .blockOptional(Duration.ofMinutes(3))
                    .orElseThrow()
                dbPath
            }.map {
                domain.withS3Id(it)
            }
        } catch (e: WebClientResponseException) {
            logger.error { e.responseBodyAsString }
            throw S3Exception(e.statusCode, e.responseBodyAsString)
        } catch (e: Exception) {
            logger.error(e) { "" }
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
        }
    }

    override fun delete(delete: PictureDomainDeleteRequest) {
        for (id in delete.domainId) {
            val path = "/${s3Properties.bucketName}/${delete.domainAddInfo}/${id.key}"
            val date = calculationService.calculateDate()
            // https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html
            val hash = calculationService.calculateSha256Hash("".toByteArray())
            try {
                s3WebClient.delete()
                    .uri { it.path(path).build() }
                    .header("x-amz-date", date)
                    .header("x-amz-content-sha256", hash)
                    .header(HttpHeaders.AUTHORIZATION, calculationService.calculateAuthorizationHeader(HttpMethod.DELETE, date, path, hash))
                    .retrieve()
                    .toBodilessEntity()
                    .blockOptional(Duration.ofMinutes(3))
                    .orElseThrow()
            } catch (e: Exception) {
                logger.error(e) { "" }
                throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
            }
        }
    }

    override fun deleteInBatch(req: DeleteFolderContentRequest) {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + XmlMapper().writeValueAsString(Delete(req.ids.map { Obj("${req.folderName}/${it}" ) }))
        logger.debug { "xml = $xml" }
        val path = "/${s3Properties.bucketName}"
        val date = calculationService.calculateDate()
        val hash = calculationService.calculateSha256Hash(xml.toByteArray())
        val delete = "delete"
        try {
            s3WebClient.post()
                .uri { it.path(path).query(delete).build() }
                .header("x-amz-date", date)
                .header("x-amz-content-sha256", hash)
                .header(HttpHeaders.AUTHORIZATION, calculationService.calculateAuthorizationHeader(HttpMethod.POST, date, path, hash, mapOf(delete to emptySet())))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE)
                .bodyValue(xml)
                .retrieve()
                .toBodilessEntity()
                .blockOptional(Duration.ofMinutes(3))
                .orElseThrow()

        } catch (e: WebClientResponseException) {
            logger.error { e.responseBodyAsString }
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.responseBodyAsString)
        } catch (e: Exception) {
            logger.error(e) { "" }
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
        }
    }

    override fun getByPath(path: String): Resource {
        val calcPath = "/${s3Properties.bucketName}/${path}"
        val date = calculationService.calculateDate()
        val hash = calculationService.calculateSha256Hash("".toByteArray())
        try {
            return s3WebClient.get()
                .uri { it.path(calcPath).build() }
                .header("x-amz-date", date)
                .header("x-amz-content-sha256", hash)
                .header("response-content-type", MediaType.APPLICATION_OCTET_STREAM_VALUE)
                .header(HttpHeaders.AUTHORIZATION, calculationService.calculateAuthorizationHeader(HttpMethod.GET, date, calcPath, hash))
                .retrieve()
                .bodyToMono<Resource>()
                .blockOptional(Duration.ofMinutes(3))
                .orElseThrow()
        } catch (e: Exception) {
            logger.error(e) { "" }
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.message)
        }
    }
}

private data class Delete(
    @field:JsonProperty("Object")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    val obj: List<Obj>,
    @field:JsonProperty("Quiet")
    val quiet: Boolean = true,
)

private data class Obj(
    @field:JsonProperty("Key")
    val key: String
)