package ru.dolmatovskaya.studio.pictures.s3.service

import org.springframework.core.io.Resource
import ru.dolmatovskaya.studio.pictures.domain.DeleteFolderContentRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainCreateRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest

interface S3Service {
    fun save(domain: PictureDomainCreateRequest): List<PictureDomainCreateRequest>
    fun delete(delete: PictureDomainDeleteRequest)
    fun getByPath(path: String): Resource
    fun deleteInBatch(req: DeleteFolderContentRequest)
}