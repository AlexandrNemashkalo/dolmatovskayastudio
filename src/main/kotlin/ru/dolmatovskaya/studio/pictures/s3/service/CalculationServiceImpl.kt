package ru.dolmatovskaya.studio.pictures.s3.service

import java.security.MessageDigest
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import mu.KotlinLogging
import org.springframework.http.HttpMethod
import org.springframework.security.crypto.codec.Hex
import org.springframework.stereotype.Service
import ru.dolmatovskaya.studio.property.S3Properties

private const val `SHA-256` = "SHA-256"
private const val Z = "Z"

private val logger = KotlinLogging.logger { }

private const val RU_CENTER = "ru-center"
private const val S3 = "s3"
private const val AWS4_REQUEST = "aws4_request"

@Service
class CalculationServiceImpl(
    private val s3Properties: S3Properties
) : CalculationService {

    override fun calculateDate(): String =
        // utc
        (ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern(s3Properties.canonicalRequest.s3DateTimePattern)) + Z)
            .also { logger.debug { "date = $it" } }

    //@formatter:off
    override fun calculateCanonicalRequest(method: HttpMethod, date: String, path: String, fileHash: String, queryParams: Map<String, Collection<String>>): String {
        val canonicalRequest = s3Properties.canonicalRequest
        val queryParamsString = "${queryParams.map { "${it.key}=${it.value.let { it.ifEmpty { "" } }}" }}".replace("[", "").replace("]", "")
        return ("${method.name}\n" +
                "${path}\n" +
                "${queryParamsString}\n" +
                "host:${canonicalRequest.host}\n" +
                "x-amz-date:${date}\n" +
                "x-amz-content-sha256:${fileHash}\n" +
                "\n" +
                "host;x-amz-date;x-amz-content-sha256\n" +
                fileHash)
            .also { logger.debug { "canon req \n$it" } }
    }
    //@formatter:on

    //@formatter:off
    override fun calculateStringToSign(date: String, canonicalRequest: String): String {
        val stringToSign = s3Properties.stringToSign
        val encodedCr = Hex.encode(MessageDigest.getInstance(`SHA-256`).digest(canonicalRequest.toByteArray()))
        return ("${stringToSign.prefix}\n" +
                "${date}\n" +
                "${date.substringBefore("T")}/$RU_CENTER/$S3/$AWS4_REQUEST\n" +
                String(encodedCr))
            .also { logger.debug { "str 2 sign = \n$it" } }
    }
    //@formatter:on

    override fun calculateSignature(date: String, stringToString: String): String {
        val signatureProp = s3Properties.signature
        val macInstanceName = signatureProp.macInstanceName

        val mac = Mac.getInstance(macInstanceName)
        mac.init(SecretKeySpec("${signatureProp.prefix}${s3Properties.secretKey}".toByteArray(), macInstanceName))

        var nextKey = mac.doFinal(date.substringBefore("T").toByteArray())
        mac.init(SecretKeySpec(nextKey, macInstanceName))

        nextKey = mac.doFinal(RU_CENTER.toByteArray())
        mac.init(SecretKeySpec(nextKey, macInstanceName))

        nextKey = mac.doFinal(S3.toByteArray())
        mac.init(SecretKeySpec(nextKey, macInstanceName))

        val targetKey = mac.doFinal(AWS4_REQUEST.toByteArray())
        mac.init(SecretKeySpec(targetKey, macInstanceName))

        val signature = mac.doFinal(stringToString.toByteArray())
        return String(Hex.encode(signature))
            .also { logger.debug { "signature = $it" } }
    }

    override fun calculateAuthorizationHeader(method: HttpMethod, date: String, path: String, fileHash: String, queryParams: Map<String, Collection<String>>): String {
        val cr = calculateCanonicalRequest(method, date, path, fileHash, queryParams)
        val sts = calculateStringToSign(date, cr)
        val signature = calculateSignature(date, sts)

        val prefix = s3Properties.stringToSign.prefix
        val keyId = s3Properties.keyId
        return ("$prefix Credential=$keyId/${date.substringBefore("T")}/$RU_CENTER/$S3/$AWS4_REQUEST," +
                "SignedHeaders=host;x-amz-date;x-amz-content-sha256,Signature=$signature")
                    .also { logger.debug { "Auth header = $it" } }
    }

    override fun calculateSha256Hash(bytes: ByteArray): String =
        String(Hex.encode(MessageDigest.getInstance(`SHA-256`).digest(bytes)))

}