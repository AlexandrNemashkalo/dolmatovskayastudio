package ru.dolmatovskaya.studio.pictures.s3.service

import org.springframework.http.HttpMethod

interface CalculationService {
    fun calculateDate(): String
    fun calculateCanonicalRequest(method: HttpMethod, date: String, path: String, fileHash: String, queryParams: Map<String, Collection<String>> = emptyMap()): String
    fun calculateStringToSign(date: String, canonicalRequest: String): String
    fun calculateSignature(date: String, stringToString: String): String
    fun calculateAuthorizationHeader(method: HttpMethod, date: String, path: String, fileHash: String, queryParams: Map<String, Collection<String>> = emptyMap()): String
    fun calculateSha256Hash(bytes: ByteArray): String
}