package ru.dolmatovskaya.studio.pictures

import java.util.UUID
import mu.KotlinLogging
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.dolmatovskaya.studio.pictures.api.PictureService
import ru.dolmatovskaya.studio.pictures.db.service.PictureDBService
import ru.dolmatovskaya.studio.pictures.domain.DeleteFolderContentRequest
import ru.dolmatovskaya.studio.pictures.domain.GetByIdAndDomainAddInfoRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainCreateRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.pictures.domain.GetByPictureUUIDRequest
import ru.dolmatovskaya.studio.pictures.domain.UpdatePictureRequest
import ru.dolmatovskaya.studio.pictures.s3.service.S3Service

private val logger = KotlinLogging.logger { }

@Service
class PictureServiceImpl(
    private val s3Service: S3Service,
    private val pictureDBService: PictureDBService,
) : PictureService {

    override fun get(picture: GetByPictureUUIDRequest): Pair<Resource, MediaType> {
        val pic = pictureDBService.get(picture)
        val s3Id = pic.s3Id
        val mediaType = when {
            s3Id.endsWith("jpeg") ||
            s3Id.endsWith("jpg")  -> MediaType.IMAGE_JPEG
            s3Id.endsWith("gif")  -> MediaType.IMAGE_GIF
            s3Id.endsWith("png")  -> MediaType.IMAGE_PNG
            else -> MediaType.APPLICATION_OCTET_STREAM
        }
        return s3Service.getByPath(s3Id) to mediaType
    }

    override fun getLight(picture: PictureDomainGetLightRequest): List<UUID> {
        return pictureDBService.getImageUUIDs(picture)
    }

    override fun getLights(pictures: List<PictureDomainGetLightRequest>): Map<Long, List<UUID>> {
        return pictureDBService.getAllByIds(pictures).groupBy({ it.domainId }, { it.id })
            .also { logger.debug { "request $pictures response $it" }  }
    }

    @Transactional
    override fun create(picture: PictureDomainCreateRequest): List<PictureDomainCreateRequest> {
        val pictures = s3Service.save(picture)
        pictureDBService.save(pictures)
        return pictures
    }

    @Transactional
    override fun update(req: UpdatePictureRequest) {
        val pictures = pictureDBService.getByIdAndDomain(GetByIdAndDomainAddInfoRequest(req.domainId, req.domainAddInfo))

        if (pictures.isNotEmpty()) {
            val idsToDeleteInS3 = pictures
                .map { it.s3Id }
                .map { it.substringAfterLast("/") }

            s3Service.deleteInBatch(DeleteFolderContentRequest("${req.domainAddInfo}/${req.domainId}", idsToDeleteInS3))
            pictureDBService.deleteByIds(pictures.map { it.id })
        }

        val s3Id = s3Service.save(PictureDomainCreateRequest(req.domainId, req.domainAddInfo, listOf(req.file))).first().s3Id
        pictureDBService.save(listOf(PictureDomainCreateRequest(req.domainId, req.domainAddInfo, listOf(req.file)).withS3Id(s3Id)))
    }

    @Transactional
    override fun delete(picture: PictureDomainDeleteRequest, deleteFolderItself: Boolean) {
        logger.debug { "picture to delete $picture" }

        for (entry in picture.domainId) {
            val domainId = entry.key
            val uuids = entry.value
            val s3Ids = (uuids
                ?.let { pictureDBService.getS3PathByUUIDs(it) }
                ?: pictureDBService.getImageS3Paths(PictureDomainGetLightRequest(domainId, picture.domainAddInfo)))
                .map { it.substringAfterLast("/") }
            logger.debug { "s3Ids = $s3Ids" }
            if (s3Ids.isNotEmpty()) {
                s3Service.deleteInBatch(DeleteFolderContentRequest("${picture.domainAddInfo}/${domainId}", s3Ids))
                if (deleteFolderItself) {
                    s3Service.delete(PictureDomainDeleteRequest(mapOf(domainId to uuids), picture.domainAddInfo))
                }
            }
        }

        pictureDBService.delete(picture)
    }

}