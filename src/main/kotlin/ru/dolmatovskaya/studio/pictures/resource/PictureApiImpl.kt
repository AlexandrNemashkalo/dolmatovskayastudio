package ru.dolmatovskaya.studio.pictures.resource

import java.util.UUID
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.PICTURES
import ru.dolmatovskaya.studio.pictures.api.PictureApi
import ru.dolmatovskaya.studio.pictures.api.PictureService
import ru.dolmatovskaya.studio.pictures.domain.GetByPictureUUIDRequest

@RestController
@RequestMapping("$API/$PICTURES")
@CrossOrigin("*")
class PictureApiImpl(
    private val pictureService: PictureService
) : PictureApi {

    @GetMapping("{domainInfo}/{id}", produces = [MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.IMAGE_GIF_VALUE])
    override fun get(@PathVariable domainInfo: String, @PathVariable id: UUID): ResponseEntity<Resource> {
        val result = pictureService.get(GetByPictureUUIDRequest(id))
        return ResponseEntity(result.first, HttpHeaders().apply { contentType = result.second }, HttpStatus.OK)
    }
}