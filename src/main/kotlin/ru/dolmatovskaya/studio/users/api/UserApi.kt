package ru.dolmatovskaya.studio.users.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Positive
import org.springframework.validation.annotation.Validated
import ru.dolmatovskaya.studio.users.dto.UpdateUserInfoDto
import ru.dolmatovskaya.studio.users.dto.UserReadDto
import ru.dolmatovskaya.studio.users.dto.UserWriteDto
import ru.dolmatovskaya.studio.users.model.User

@Validated
interface UserApi {

    @Operation(
        description = "Создать пользователя",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "FORBIDDEN", responseCode = "403")
        ]
    )
    fun createNewUser(@Valid @RequestBody dto: UserWriteDto)


    @Operation(
        description = "Получить список пользователей",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "FORBIDDEN", responseCode = "403")
        ]
    )
    fun get(): List<UserReadDto>


    @Operation(
        description = "Обновить данные пользователя. Пользователь может обновить только свои данные ",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "FORBIDDEN", responseCode = "403")
        ]
    )
    fun update(@Positive @Parameter(`in` = ParameterIn.PATH) id: Int, @Valid @RequestBody dto: UpdateUserInfoDto, @Parameter(hidden = true) user: User)

    @Operation(
        description = "Удалить пользователя",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "FORBIDDEN", responseCode = "403")
        ]
    )
    fun delete(@Positive @Parameter(`in` = ParameterIn.PATH) id: Int, @Parameter(hidden = true) user: User)

    @Operation(
        description = "Добавить роль пользователю.",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "FORBIDDEN", responseCode = "403")
        ]
    )
    fun updateRoles(@Positive @Parameter(`in` = ParameterIn.PATH) id: Int, @NotBlank @Parameter(`in` = ParameterIn.PATH) role: String, @Parameter(hidden = true) user: User)

    @Operation(
        description = "Удалить роль пользователя.",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "FORBIDDEN", responseCode = "403")
        ]
    )
    fun deleteRole(@Positive @Parameter(`in` = ParameterIn.PATH) id: Int, @NotBlank @Parameter(`in` = ParameterIn.PATH) role: String, @Parameter(hidden = true) user: User)
}