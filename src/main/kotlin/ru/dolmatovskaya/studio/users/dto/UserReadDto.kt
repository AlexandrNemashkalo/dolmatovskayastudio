package ru.dolmatovskaya.studio.users.dto

data class UserReadDto(
    val id: Int,
    val login: String,
    val fio: String,
    val roles: Set<String>
)