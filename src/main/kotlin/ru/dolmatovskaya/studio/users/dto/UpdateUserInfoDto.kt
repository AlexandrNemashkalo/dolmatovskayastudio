package ru.dolmatovskaya.studio.users.dto

import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class UpdateUserInfoDto(
    @field:Size(max = 128)
    val fio: String? = null,
    @field:Pattern(regexp = "^[(\\w)!?_*\\)\\($\\+\\=\\-\\%\\@field:]{6,18}$")
    @field:Size(max = 18, min = 6)
    val password: String? = null
)