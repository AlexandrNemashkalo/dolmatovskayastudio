package ru.dolmatovskaya.studio.users.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class UserWriteDto(
    @field:NotBlank
    @field:Size(max = 64)
    val login: String,
    @field:NotBlank
    @field:Size(max = 128)
    val fio: String,
    @field:NotBlank
    @field:Pattern(regexp = "^[(\\w)!?_*\\)\\($\\+\\=\\-\\%\\@]{6,18}$")
    @field:Size(max = 18, min = 6)
    val password: String,
    @field:Size(min = 1)
    val roles: Set<@NotBlank String>
)