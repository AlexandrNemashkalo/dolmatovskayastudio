package ru.dolmatovskaya.studio.users.resource

import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.ROLES
import ru.dolmatovskaya.studio.consts.Endpoints.USERS
import ru.dolmatovskaya.studio.users.api.UserApi
import ru.dolmatovskaya.studio.users.dto.UpdateUserInfoDto
import ru.dolmatovskaya.studio.users.dto.UserReadDto
import ru.dolmatovskaya.studio.users.dto.UserWriteDto
import ru.dolmatovskaya.studio.users.model.User
import ru.dolmatovskaya.studio.users.service.UserService

private val log = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$USERS")
@CrossOrigin("*")
class UserApiImpl(
    private val userService: UserService
): UserApi {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    override fun createNewUser(@RequestBody dto: UserWriteDto) {
        userService.createNewUser(dto)
    }

    @GetMapping
    override fun get(): List<UserReadDto> {
        return userService.get()
    }

    @PutMapping("{id}")
    override fun update(@PathVariable id: Int, @RequestBody dto: UpdateUserInfoDto, @AuthenticationPrincipal user: User) {
        log.info { "Update user by $user $id $dto" }
        userService.update(id, dto, user)
    }

    @DeleteMapping("{id}")
    override fun delete(@PathVariable id: Int, @AuthenticationPrincipal user: User) {
        log.info { "Delete by $user $id" }
        userService.delete(id, user)
    }

    @PatchMapping("{id}/$ROLES/{role}")
    override fun updateRoles(@PathVariable id: Int, @PathVariable role: String, @AuthenticationPrincipal user: User) {
        log.info { "Update roles $user $id $role" }
        userService.updateRoles(id, role)
    }

    @DeleteMapping("{id}/$ROLES/{role}")
    override fun deleteRole(@PathVariable id: Int, @PathVariable role: String, @AuthenticationPrincipal user: User) {
        log.info { "Delete roles $user $id $role" }
        userService.deleteRole(id, role, user)
    }

}