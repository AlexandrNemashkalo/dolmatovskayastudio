package ru.dolmatovskaya.studio.users.model

import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import ru.dolmatovskaya.studio.converter.RolesConverter

@Entity
@Table(name = "users")
class User(
    val login: String,
    val fio: String,
    val pass: String,
    @Convert(converter = RolesConverter::class)
    val roles: Set<String>,
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_gen")
    @SequenceGenerator(name = "users_id_gen", sequenceName = "users_id_seq", allocationSize = 1)
    val id: Int? = null,
) : UserDetails {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = roles.map { GrantedAuthority { it } }.toMutableList()

    override fun getPassword(): String = pass
    override fun getUsername(): String = login

    override fun isAccountNonExpired(): Boolean = true
    override fun isAccountNonLocked(): Boolean = true
    override fun isCredentialsNonExpired(): Boolean = true
    override fun isEnabled(): Boolean = true

    override fun toString(): String {
        return "User(login='$login', fio='$fio', roles=$roles)"
    }
}