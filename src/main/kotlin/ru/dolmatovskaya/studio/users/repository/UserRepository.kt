package ru.dolmatovskaya.studio.users.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.transaction.annotation.Transactional
import ru.dolmatovskaya.studio.users.model.User

interface UserRepository : JpaRepository<User, Int> {
    @Modifying
    @Query("update public.users u set roles = :roles where u.id = :id", nativeQuery = true)
    @Transactional
    fun updateRoles(@Param("id") id: Int, @Param("roles") roles: String)

    @Query(value = "select count(*) from public.users u where u.roles like %:role%", nativeQuery = true)
    fun countAllByRolesContains(@Param("role") role: String): Int

    fun findUserByLogin(login: String): User?

    @Modifying
    @Query("update User u set u.fio = :fio where u.id = :id")
    @Transactional
    fun updateUserFio(@Param("id") id: Int, @Param("fio") fio: String)

    @Modifying
    @Query("update User u set u.pass = :pass where u.id = :id")
    @Transactional
    fun updateUserPassword(@Param("id") id: Int, @Param("pass") password: String)

}