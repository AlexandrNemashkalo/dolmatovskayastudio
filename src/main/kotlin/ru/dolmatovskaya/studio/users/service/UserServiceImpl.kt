package ru.dolmatovskaya.studio.users.service

import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.consts.Roles
import ru.dolmatovskaya.studio.converter.RolesConverter
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.users.dto.UpdateUserInfoDto
import ru.dolmatovskaya.studio.users.dto.UserReadDto
import ru.dolmatovskaya.studio.users.dto.UserWriteDto
import ru.dolmatovskaya.studio.users.model.User
import ru.dolmatovskaya.studio.users.repository.UserRepository

@Service
class UserServiceImpl(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder,
    private val rolesConverter: RolesConverter,
): UserService {

    override fun createNewUser(dto: UserWriteDto) {
        val foundUser = userRepository.findUserByLogin(dto.login)
        if (foundUser != null) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "User with login ${dto.login} already exists").andLog()
        } else {
            userRepository.save(User(dto.login, dto.fio, passwordEncoder.encode(dto.password), dto.roles))
        }
    }

    override fun get(): List<UserReadDto> {
        return userRepository.findAll().map {
            UserReadDto(it.id!!, it.login, it.fio, it.roles)
        }
    }

    override fun update(id: Int, dto: UpdateUserInfoDto, user: User) {
        if (id == user.id) {
            if (dto.fio != null) {
                userRepository.updateUserFio(user.id, dto.fio)
            }
            if (dto.password != null) {
                userRepository.updateUserPassword(user.id, passwordEncoder.encode(dto.password))
            }
        }
    }

    override fun delete(id: Int, user: User) {
        val allSuperAdmins = userRepository.countAllByRolesContains(Roles.SUPER_ADMIN)
        if (allSuperAdmins <= 1 && user.roles.contains(Roles.SUPER_ADMIN) && id == user.id ) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "At least 1 super admin should be in system").andLog()
        } else {
            userRepository.deleteById(id)
        }
    }

    override fun updateRoles(id: Int, role: String) {
        val user = userRepository.findById(id).orElseThrow { throw ResponseStatusException(HttpStatus.NOT_FOUND, "$id").andLog() }
        userRepository.updateRoles(id, rolesConverter.convertToDatabaseColumn(user.roles + role))
    }

    override fun deleteRole(id: Int, role: String, user: User) {
        val superAdmins = userRepository.countAllByRolesContains(role)
        if (superAdmins <= 1 && role == Roles.SUPER_ADMIN && user.id == id) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "At least 1 super admin should be in system").andLog()
        } else {
            if (role != Roles.SUPER_ADMIN) {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Only ${Roles.SUPER_ADMIN} can be revoked. If you want to revoke ${Roles.ADMIN} then delete user").andLog()
            }
            val userToUpdate = userRepository.findById(id).orElseThrow { throw ResponseStatusException(HttpStatus.NOT_FOUND, "$id").andLog() }
            userRepository.updateRoles(id, rolesConverter.convertToDatabaseColumn(userToUpdate.roles - role))
        }
    }
}