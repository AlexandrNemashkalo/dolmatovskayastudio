package ru.dolmatovskaya.studio.users.service

import ru.dolmatovskaya.studio.users.dto.UpdateUserInfoDto
import ru.dolmatovskaya.studio.users.dto.UserReadDto
import ru.dolmatovskaya.studio.users.dto.UserWriteDto
import ru.dolmatovskaya.studio.users.model.User

interface UserService {
    fun createNewUser(dto: UserWriteDto)
    fun get(): List<UserReadDto>
    fun update(id: Int, dto: UpdateUserInfoDto, user: User)
    fun delete(id: Int, user: User)
    fun updateRoles(id: Int, role: String)
    fun deleteRole(id: Int, role: String, user: User)
}