package ru.dolmatovskaya.studio.auth.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.validation.Valid
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.RequestBody
import ru.dolmatovskaya.studio.auth.dto.AuthRequestDto
import ru.dolmatovskaya.studio.auth.dto.AuthResponseDto
import ru.dolmatovskaya.studio.auth.dto.CheckTokenRequest
import ru.dolmatovskaya.studio.auth.dto.CheckTokenResponse

@Validated
interface AuthApi {
    @Operation(
        description = "Получить токен по логину и паролю",
        summary = "Для неавторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "FORBIDDEN", responseCode = "403"),
        ]
    )
    fun auth(@Valid @RequestBody dto: AuthRequestDto): AuthResponseDto

    @Operation(
        description = "Проверить, валиден ли токен",
        summary = "Для неавторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
        ]
    )
    fun checkToken(@Valid @RequestBody dto: CheckTokenRequest): CheckTokenResponse
}