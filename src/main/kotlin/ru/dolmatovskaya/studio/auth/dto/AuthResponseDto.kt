package ru.dolmatovskaya.studio.auth.dto

data class AuthResponseDto(
    val jwt: String
)