package ru.dolmatovskaya.studio.auth.dto

data class CheckTokenResponse(
    val isValid: Boolean
)
