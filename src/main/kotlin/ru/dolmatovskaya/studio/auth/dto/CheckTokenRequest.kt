package ru.dolmatovskaya.studio.auth.dto

import javax.validation.constraints.NotBlank

data class CheckTokenRequest(
    @field:NotBlank
    val jwt: String
)
