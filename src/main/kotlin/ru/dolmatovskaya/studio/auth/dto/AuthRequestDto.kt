package ru.dolmatovskaya.studio.auth.dto

import javax.validation.constraints.NotBlank

data class AuthRequestDto(
    @field:NotBlank
    val login: String,
    @field:NotBlank
    val password: String
)