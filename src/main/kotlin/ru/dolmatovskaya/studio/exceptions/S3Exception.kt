package ru.dolmatovskaya.studio.exceptions

import org.springframework.http.HttpStatus

data class S3Exception(val code: HttpStatus, val body: String) : RuntimeException()