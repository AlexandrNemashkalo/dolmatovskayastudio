package ru.dolmatovskaya.studio.security

import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.Date
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service

private val logger = KotlinLogging.logger { }

@Service
class JwtServiceImpl(
    @Value("\${secret}")
    private val secret: String,
    @Value("\${token-expiration-time:8}")
    private val tokenExpirationTime: Long
): JwtService {

    override fun generateToken(userDetails: UserDetails): String  {
        val from = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant())
        val expDate = Date.from(LocalDateTime.now().plusHours(tokenExpirationTime).atZone(ZoneId.systemDefault()).toInstant())

        var token = ""
        try {
            token = Jwts.builder()
                .setSubject(userDetails.username)
                .setIssuedAt(from)
                .setNotBefore(from)
                .setExpiration(expDate)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact()
        } catch (e: JwtException) {
            e.printStackTrace()
        }
         return token
    }

    override fun extractUsername(token: String): String? {
        return Jwts.parser()
            .setSigningKey(secret)
            .parseClaimsJws(token)
            .body
            .subject
    }

    override fun isTokenValid(token: String, userDetails: UserDetails): Boolean {
        val claims = Jwts.parser()
            .setSigningKey(secret)
            .parseClaimsJws(token)
            .body
        return userDetails.username == claims.subject
                && LocalDateTime.now().atZone(ZoneOffset.systemDefault()) < claims.expiration.toInstant().atZone(ZoneOffset.systemDefault())
    }

    override fun isTokenExpired(token: String): Boolean {
        return try {
            val expirationDate = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .body
                .expiration
            val tokenExpirationTime = expirationDate.toInstant().atZone(ZoneId.systemDefault())
            tokenExpirationTime < ZonedDateTime.now()
        } catch (e: Exception) {
            logger.error(e) { }
            true
        }
    }
}