package ru.dolmatovskaya.studio.security

import org.springframework.security.core.userdetails.UserDetails

interface JwtService {
    fun generateToken(userDetails: UserDetails): String
    fun extractUsername(token: String): String?
    fun isTokenValid(token: String, userDetails: UserDetails): Boolean
    fun isTokenExpired(token: String): Boolean
}