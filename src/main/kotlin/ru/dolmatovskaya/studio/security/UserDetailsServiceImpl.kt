package ru.dolmatovskaya.studio.security

import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.users.repository.UserRepository

@Service
class UserDetailsServiceImpl(
    private val userRepository: UserRepository
) : UserDetailsService {
    override fun loadUserByUsername(login: String): UserDetails {
        return userRepository.findUserByLogin(login) ?: throw UsernameNotFoundException(login).andLog()
    }
}