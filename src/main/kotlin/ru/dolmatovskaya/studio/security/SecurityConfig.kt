package ru.dolmatovskaya.studio.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import ru.dolmatovskaya.studio.consts.Endpoints
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.APPROVE
import ru.dolmatovskaya.studio.consts.Endpoints.AUTHENTICATE
import ru.dolmatovskaya.studio.consts.Endpoints.BOOKING
import ru.dolmatovskaya.studio.consts.Endpoints.CAROUSEL
import ru.dolmatovskaya.studio.consts.Endpoints.CONTACTS
import ru.dolmatovskaya.studio.consts.Endpoints.FILTERS
import ru.dolmatovskaya.studio.consts.Endpoints.FINISH
import ru.dolmatovskaya.studio.consts.Endpoints.PROJECTS
import ru.dolmatovskaya.studio.consts.Endpoints.RESERVATION
import ru.dolmatovskaya.studio.consts.Endpoints.ROLES
import ru.dolmatovskaya.studio.consts.Endpoints.STUFF
import ru.dolmatovskaya.studio.consts.Endpoints.USERS
import ru.dolmatovskaya.studio.consts.Roles
import ru.dolmatovskaya.studio.filters.JwtFilter
import ru.dolmatovskaya.studio.property.CorsProperties

@Configuration
@EnableWebSecurity
@Profile("prod")
class SecurityConfig(
    private val corsProperties: CorsProperties,
    private val jwtFilter: JwtFilter
) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http
            .csrf().disable()
            .cors().disable()
            .authorizeRequests()
            // login
            .antMatchers(HttpMethod.POST, "/$API/$AUTHENTICATE**").permitAll()
            //create new user
            .antMatchers(HttpMethod.POST, "/$API/$USERS").hasAnyAuthority(Roles.SUPER_ADMIN, Roles.GOD)
            // get all users
            .antMatchers(HttpMethod.GET, "/$API/$USERS").hasAnyAuthority(Roles.SUPER_ADMIN, Roles.GOD)
            //delete user
            .antMatchers(HttpMethod.DELETE, "/$API/$USERS/*").hasAnyAuthority(Roles.SUPER_ADMIN, Roles.GOD)
            // update user info
            .antMatchers(HttpMethod.PUT, "/$API/$USERS/*").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            // updates user's roles
            .antMatchers(HttpMethod.PATCH, "/$API/$USERS/*/$ROLES/*").hasAnyAuthority(Roles.SUPER_ADMIN, Roles.GOD)
            // remove
            .antMatchers(HttpMethod.DELETE, "/$API/$USERS/*/$ROLES/*").hasAnyAuthority(Roles.SUPER_ADMIN, Roles.GOD)

            // create new projects and so on
            .antMatchers(HttpMethod.POST, "/$API/$PROJECTS**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.PUT, "/$API/$PROJECTS**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.DELETE, "/$API/$PROJECTS**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)

            // create new projects and so on
            .antMatchers(HttpMethod.GET, "/$API/$RESERVATION/$BOOKING**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.DELETE, "/$API/$RESERVATION/$BOOKING**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.POST, "/$API/$RESERVATION/$BOOKING/*/$FINISH").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.POST, "/$API/$RESERVATION/$BOOKING/*/$APPROVE").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.POST, "/$API/$RESERVATION/$BOOKING").permitAll()
            .antMatchers(HttpMethod.POST, "/$API/$RESERVATION/$STUFF").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.GET, "/$API/$RESERVATION/$STUFF/$FILTERS**").permitAll()
            .antMatchers(HttpMethod.PUT, "/$API/$RESERVATION**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.DELETE, "/$API/$RESERVATION**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)

            .antMatchers(HttpMethod.DELETE, "$API/$FILTERS**").hasAnyAuthority(Roles.SUPER_ADMIN, Roles.GOD, Roles.ADMIN)
            .antMatchers(HttpMethod.PUT, "$API/$FILTERS**").hasAnyAuthority(Roles.SUPER_ADMIN, Roles.GOD, Roles.ADMIN)
            .antMatchers(HttpMethod.POST, "$API/$FILTERS**").hasAnyAuthority(Roles.SUPER_ADMIN, Roles.GOD, Roles.ADMIN)
            .antMatchers(HttpMethod.GET, "$API/$FILTERS**").permitAll()

            // contacts api
            .antMatchers(HttpMethod.POST, "/$API/$CONTACTS**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.PUT, "/$API/$CONTACTS**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.DELETE, "/$API/$CONTACTS**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)

            .antMatchers(HttpMethod.POST, "$API/$CAROUSEL**").hasAnyAuthority(Roles.ADMIN, Roles.SUPER_ADMIN, Roles.GOD)
            .antMatchers(HttpMethod.GET, "$API/$CAROUSEL**").permitAll()

            .antMatchers(HttpMethod.GET, "/swagger-ui**").hasAnyAuthority(Roles.DEV, Roles.GOD)
            .antMatchers(HttpMethod.GET, "/v3/$API**").hasAnyAuthority(Roles.DEV , Roles.GOD)
            .antMatchers(HttpMethod.GET, "/$API/$PROJECTS**").permitAll()
            .and()
            .formLogin().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter::class.java)
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder()
    }
}