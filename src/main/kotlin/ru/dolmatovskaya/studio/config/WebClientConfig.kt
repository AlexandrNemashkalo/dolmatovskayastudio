package ru.dolmatovskaya.studio.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.reactive.function.client.ExchangeStrategies
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient
import ru.dolmatovskaya.studio.property.S3Properties
import ru.dolmatovskaya.studio.property.TelegramProperties

@Configuration
class WebClientConfig(
    val telegramProperties: TelegramProperties,
    @Value("\${max-size-mb:25}")
    private val maxSizeMB: Int,
    private val s3Properties: S3Properties
) {

    private val prepareBuilder: WebClient.Builder = WebClient
        .builder()
        .clientConnector(ReactorClientHttpConnector(HttpClient.create().wiretap(true)))

    @Bean
    fun telegramWebClient(): WebClient = prepareBuilder
        .baseUrl(telegramProperties.baseUrl)
        .build()

    @Bean
    fun s3WebClient(): WebClient {
        return WebClient.builder()
            .baseUrl(s3Properties.basePath)
            .exchangeStrategies(
                ExchangeStrategies.builder()
                    .codecs { it.defaultCodecs().maxInMemorySize(maxSizeMB * 1024 * 1024) }
                    .build()
            ).clientConnector(ReactorClientHttpConnector(HttpClient.create().wiretap(true)))
            .build()
    }
}