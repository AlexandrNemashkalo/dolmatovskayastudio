package ru.dolmatovskaya.studio.consts

object Roles {
    const val SUPER_ADMIN = "SUPER_ADMIN"
    const val ADMIN = "ADMIN"
    const val GOD = "GOD"
    const val DEV = "DEV"

    val ALL = setOf(SUPER_ADMIN, ADMIN, GOD, DEV)
}