package ru.dolmatovskaya.studio.consts

object Domains {
    const val FILMS = "films"
    const val COLLABORATIONS = "collaborations"
    const val COMMERCIALS = "commercials"
    const val MUSIC_VIDEOS = "music_videos"
    const val MEDIA = "media"
    const val THEATERS = "theaters"
}