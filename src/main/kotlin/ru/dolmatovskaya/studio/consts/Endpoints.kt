package ru.dolmatovskaya.studio.consts

object Endpoints {
    const val API = "api"
    const val USERS = "users"
    const val ROLES = "roles"
    const val AUTHENTICATE = "authenticate"

    const val CONTACTS = "contacts"
    const val PICTURES = "pictures"

    const val PROJECTS = "projects"
    const val COLLABORATIONS = "collaborations"
    const val COMMERCIALS = "commercials"
    const val FILMS = "films"
    const val MEDIA = "media"
    const val MUSIC_VIDEOS = "music_videos"
    const val THEATERS = "theaters"

    const val RESERVATION = "reservation"
    const val STUFF = "stuff"
    const val BOOKING = "booking"
    const val FINISH = "finish"
    const val APPROVE = "approve"
    const val FILTERS = "filters"
    const val CAROUSEL = "carousel"
}