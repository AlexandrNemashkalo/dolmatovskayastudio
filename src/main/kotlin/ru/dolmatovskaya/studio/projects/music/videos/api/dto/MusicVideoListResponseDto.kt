package ru.dolmatovskaya.studio.projects.music.videos.api.dto

data class MusicVideoListResponseDto(
    val totalCount: Long,
    val value: List<MusicVideoResponseDto> = emptyList()
) {
    companion object {
        fun empty() = MusicVideoListResponseDto(0)
    }
}
