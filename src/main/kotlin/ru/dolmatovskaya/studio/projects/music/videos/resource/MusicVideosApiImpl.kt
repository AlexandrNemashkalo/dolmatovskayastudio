package ru.dolmatovskaya.studio.projects.music.videos.resource

import mu.KotlinLogging
import org.springframework.http.MediaType
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.MUSIC_VIDEOS
import ru.dolmatovskaya.studio.consts.Endpoints.PROJECTS
import ru.dolmatovskaya.studio.projects.music.videos.api.MusicVideosApi
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoCreateRequestDto
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoListResponseDto
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoResponseDto
import ru.dolmatovskaya.studio.projects.music.videos.service.MusicVideoService
import ru.dolmatovskaya.studio.users.model.User

private val log = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$PROJECTS/$MUSIC_VIDEOS")
@CrossOrigin("*")
class MusicVideosApiImpl(
    private val musicService: MusicVideoService
) : MusicVideosApi {
    @PostMapping
    override fun create(@RequestBody dto: MusicVideoCreateRequestDto, @AuthenticationPrincipal user: User): MusicVideoResponseDto {
        log.info { "Create by $user $dto" }
        return musicService.create(dto)
    }

    @GetMapping("{id}")
    override fun get(@PathVariable id: Long): MusicVideoResponseDto {
        return musicService.getById(id)
    }

    @GetMapping
    override fun getAll(
        @RequestParam(required = false, defaultValue = "0") page: Int,
        @RequestParam(required = false, defaultValue = "25") size: Int
    ): MusicVideoListResponseDto {
        return musicService.getAll(page, size)
    }

    @PutMapping("{id}")
    override fun update(@PathVariable id: Long, @RequestBody dto: MusicVideoCreateRequestDto, @AuthenticationPrincipal user: User): MusicVideoResponseDto {
        log.info { "Update $user $id $dto" }
        return musicService.update(dto.withId(id))
    }

    @DeleteMapping
    override fun delete(@RequestParam ids: Set<Long>, @AuthenticationPrincipal user: User) {
        log.info { "Delete by $user $ids" }
        return musicService.delete(ids)
    }

    @PostMapping("{id}/preview_picture", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    override fun updatePosterPicture(@PathVariable id: Long, @RequestParam("file") file: MultipartFile, @AuthenticationPrincipal user: User) {
        log.info { "Update pic $user $id" }
        musicService.updatePosterPicture(id, file)
    }
}