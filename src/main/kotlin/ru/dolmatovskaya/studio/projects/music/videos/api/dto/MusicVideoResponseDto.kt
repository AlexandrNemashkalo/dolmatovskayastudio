package ru.dolmatovskaya.studio.projects.music.videos.api.dto

data class MusicVideoResponseDto(
    val id: Long,
    val pictureUrl: String? = null,
    val musicVideoUrl: String,
    val nameRus: String,
    val nameEng: String,
    val order: Long,
) {
    fun withPictureUrl(previewUrl: String) = MusicVideoResponseDto(id, previewUrl, musicVideoUrl, nameRus, nameEng, order)
}