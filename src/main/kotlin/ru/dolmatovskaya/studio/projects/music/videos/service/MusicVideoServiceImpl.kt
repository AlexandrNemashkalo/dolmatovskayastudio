package ru.dolmatovskaya.studio.projects.music.videos.service

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.MUSIC_VIDEOS
import ru.dolmatovskaya.studio.consts.Endpoints.PICTURES
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.extensions.orElseThrowNotFound
import ru.dolmatovskaya.studio.pictures.api.PictureService
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.pictures.domain.UpdatePictureRequest
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoCreateRequestDto
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoListResponseDto
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoResponseDto
import ru.dolmatovskaya.studio.projects.music.videos.mapper.MusicVideoMapper
import ru.dolmatovskaya.studio.projects.music.videos.model.MusicVideo
import ru.dolmatovskaya.studio.projects.music.videos.repository.MusicVideoRepository
import ru.dolmatovskaya.studio.property.ServerProperties

@Service
class MusicVideoServiceImpl(
    private val musicVideoRepository: MusicVideoRepository,
    private val musicVideoMapper: MusicVideoMapper,
    private val pictureService: PictureService,
    private val serverProperties: ServerProperties
) : MusicVideoService {
    override fun create(dto: MusicVideoCreateRequestDto): MusicVideoResponseDto {
        return musicVideoMapper.fromEntity(musicVideoRepository.save(musicVideoMapper.fromDto(dto)))
    }

    override fun getById(id: Long): MusicVideoResponseDto {
        return musicVideoRepository.findById(id)
            .map { musicVideoMapper.fromEntity(it) }
            .map {
                if (it.pictureUrl != null) {
                    it
                } else {
                    val pictureId = pictureService.getLight(PictureDomainGetLightRequest.musicVideo(id)).firstOrNull()
                    val url = pictureId?.let { "${serverProperties.hostname}/$API/$PICTURES/$MUSIC_VIDEOS/$it" }
                    url?.let { u -> it.withPictureUrl(u) } ?: it
                }
            }
            .orElseThrowNotFound("music video with id $id not found")
    }

    override fun getAll(page: Int, size: Int): MusicVideoListResponseDto {
        val sort = Sort.by(Sort.Order.asc("order"))
        val answer = if (size == -1) {
            musicVideoRepository.findAll(sort).let { it to it.size.toLong() }
        } else {
            val musicVideos = musicVideoRepository.findAll(PageRequest.of(page, size, sort))

            musicVideos.content to musicVideos.totalElements
        }
        val musicVideos = answer.first

        if (musicVideos.isEmpty()) {
            return MusicVideoListResponseDto.empty()
        }

        val map = pictureService.getLights(musicVideos.map { PictureDomainGetLightRequest.musicVideo(it.id!!) })

        val dtos = musicVideoMapper.fromEntities(musicVideos)

        val result = if (map.isNotEmpty()) {
            dtos.map {
                if (it.pictureUrl != null) {
                    it
                } else {
                    val picId = map[it.id]?.firstOrNull()
                    if (picId != null) {
                        it.withPictureUrl("${serverProperties.hostname}/$API/$PICTURES/$MUSIC_VIDEOS/${picId}")
                    } else {
                        it
                    }
                }
            }
        } else {
            dtos
        }

        return MusicVideoListResponseDto(answer.second, result.sortedBy { it.order })
    }

    @Transactional
    override fun update(dto: MusicVideoCreateRequestDto): MusicVideoResponseDto {
        if (dto.pictureUrl != null) {
            pictureService.delete(PictureDomainDeleteRequest.musicVideo(setOf(dto.id!!)))
        }
        val fromDto = musicVideoMapper.fromDto(dto)
        val result = if (fromDto.id == null) {
            save(fromDto, musicVideoRepository.findMaxOrder() + 1)
        } else {
            val fromDB = musicVideoRepository.getById(fromDto.id)
            if (fromDB.order != fromDto.order) {
                save(fromDto, fromDB.order ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Должен быть указан порядок для обновления сущности ${dto.id}").andLog())
            } else {
                musicVideoRepository.save(fromDto)
            }
        }
        return musicVideoMapper.fromEntity(result)
    }

    private fun save(
        fromDto: MusicVideo,
        orderToExisting: Long
    ): MusicVideo {
        return if (fromDto.order == null) {
            musicVideoRepository.save(fromDto.apply { order = orderToExisting })
        } else {
            musicVideoRepository.findByOrder(fromDto.order!!)?.let {
                it.order = orderToExisting
                musicVideoRepository.saveAll(listOf(it, fromDto))
                fromDto
            } ?: musicVideoRepository.save(fromDto)
        }
    }

    @Transactional
    override fun delete(ids: Set<Long>) {
        pictureService.delete(PictureDomainDeleteRequest.musicVideo(ids))
        musicVideoRepository.deleteAllByIdInBatch(ids)
    }

    override fun updatePosterPicture(id: Long, file: MultipartFile) {
        if (!musicVideoRepository.existsById(id)) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "music video with id $id not found").andLog()
        }
        pictureService.update(UpdatePictureRequest.musicVideo(id, file))
    }
}