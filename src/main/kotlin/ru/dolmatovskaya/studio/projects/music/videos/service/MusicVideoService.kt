package ru.dolmatovskaya.studio.projects.music.videos.service

import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoCreateRequestDto
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoListResponseDto
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoResponseDto

interface MusicVideoService {
    fun create(dto: MusicVideoCreateRequestDto): MusicVideoResponseDto
    fun getAll(page: Int, size: Int): MusicVideoListResponseDto
    fun update(dto: MusicVideoCreateRequestDto): MusicVideoResponseDto
    fun delete(ids: Set<Long>)
    fun getById(id: Long): MusicVideoResponseDto
    fun updatePosterPicture(id: Long, file: MultipartFile)
}