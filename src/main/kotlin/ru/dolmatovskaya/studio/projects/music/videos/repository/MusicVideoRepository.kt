package ru.dolmatovskaya.studio.projects.music.videos.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.dolmatovskaya.studio.projects.music.videos.model.MusicVideo

interface MusicVideoRepository : JpaRepository<MusicVideo, Long> {
    fun findByOrder(order: Long): MusicVideo?
    @Query("select max(c.\"order\") from music_videos c", nativeQuery = true)
    fun findMaxOrder(): Long
}