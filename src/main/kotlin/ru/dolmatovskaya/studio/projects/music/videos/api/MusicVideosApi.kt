package ru.dolmatovskaya.studio.projects.music.videos.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero
import org.springframework.validation.annotation.Validated
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoCreateRequestDto
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoListResponseDto
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoResponseDto
import ru.dolmatovskaya.studio.users.model.User

@Validated
interface MusicVideosApi {
    @Operation(
        description = "Создать запись о клипе",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "Created", responseCode = "201")]
    )
    fun create(@Valid @RequestBody dto: MusicVideoCreateRequestDto, @Parameter(hidden = true) user: User): MusicVideoResponseDto

    @Operation(
        description = "Клип по id",
        summary = "Для неавторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun get(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long): MusicVideoResponseDto

    @Operation(
        description = "Все клипы",
        summary = "Для неавторизованных. Если size = -1, то вернутся все записи",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun getAll(
        @PositiveOrZero @Parameter(`in` = ParameterIn.QUERY, required = false) page: Int = 0,
        @Min(-1) @Parameter(`in` = ParameterIn.QUERY, required = false) size: Int = 25
    ): MusicVideoListResponseDto

    @Operation(
        description = "Обновить все поля по записи о клипе",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun update(@Parameter(`in` = ParameterIn.PATH) id: Long, @Valid @RequestBody dto: MusicVideoCreateRequestDto, @Parameter(hidden = true) user: User): MusicVideoResponseDto

    @Operation(
        description = "Удалить записи о клипе",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun delete(@Parameter(`in` = ParameterIn.QUERY) @NotEmpty ids: Set<Long>, @Parameter(hidden = true) user: User)

    @Operation(
        description = "Изменить превью у клипа",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun updatePosterPicture(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long, file: MultipartFile, @Parameter(hidden = true) user: User)
}