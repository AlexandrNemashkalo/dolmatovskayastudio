package ru.dolmatovskaya.studio.projects.music.videos.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoCreateRequestDto
import ru.dolmatovskaya.studio.projects.music.videos.api.dto.MusicVideoResponseDto
import ru.dolmatovskaya.studio.projects.music.videos.model.MusicVideo

@Mapper
interface MusicVideoMapper {
    fun fromDto(dto: MusicVideoCreateRequestDto): MusicVideo
    fun fromEntity(entity: MusicVideo): MusicVideoResponseDto
    fun fromEntities(entities: Collection<MusicVideo>): List<MusicVideoResponseDto>
}