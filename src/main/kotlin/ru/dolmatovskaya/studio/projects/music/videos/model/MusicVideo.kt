package ru.dolmatovskaya.studio.projects.music.videos.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import org.hibernate.Hibernate

@Entity
@Table(name = "music_videos")
data class MusicVideo(
    val nameRus: String,
    val nameEng: String,
    val musicVideoUrl: String,
    val pictureUrl: String? = null,
    @Column(name = "\"order\"")
    var order: Long? = null,
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "music_video_id_gen")
    @SequenceGenerator(name = "music_video_id_gen", sequenceName = "music_video_id_seq", allocationSize = 1)
    val id: Long? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as MusicVideo

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , nameRus = $nameRus , nameEng = $nameEng , musicVideoUrl = $musicVideoUrl, pictureUrl = $pictureUrl)"
    }
}