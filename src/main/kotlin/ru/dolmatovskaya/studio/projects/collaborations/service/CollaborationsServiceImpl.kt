package ru.dolmatovskaya.studio.projects.collaborations.service

import java.util.UUID
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.consts.Domains
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.extensions.orElseThrowNotFound
import ru.dolmatovskaya.studio.pictures.api.PictureService
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainCreateRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationCreateRequestDto
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationListResponseDto
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationResponseDto
import ru.dolmatovskaya.studio.projects.collaborations.mapper.CollaborationMapper
import ru.dolmatovskaya.studio.projects.collaborations.model.Collaboration
import ru.dolmatovskaya.studio.projects.collaborations.repository.CollaborationRepository
import ru.dolmatovskaya.studio.property.ServerProperties

@Service
class CollaborationsServiceImpl(
    private val collaborationRepository: CollaborationRepository,
    private val collaborationMapper: CollaborationMapper,
    private val pictureService: PictureService,
    private val serverProperties: ServerProperties
) : CollaborationsService {

    @Transactional
    override fun create(dto: CollaborationCreateRequestDto): CollaborationResponseDto = update(dto)

    override fun getAll(page: Int, size: Int): CollaborationListResponseDto {
        val sort = Sort.by(Sort.Order.asc("order"))
        val answer = if (size == -1) {
            collaborationRepository.findAll(sort).let { it to it.size.toLong() }
        } else {
            collaborationRepository.findAll(PageRequest.of(page, size, sort)).let { it.content to it.totalElements }
        }

        val collaborations = answer.first

        if (collaborations.isEmpty()) {
            return CollaborationListResponseDto.empty()
        }

        val map = pictureService.getLights(collaborations.map { PictureDomainGetLightRequest.collaboration(it.id!!) })

        val dtos = collaborationMapper.fromEntities(collaborations)

        val result = if (map.isNotEmpty()) {
            dtos.map {
                val picIds = map[it.id]
                if (!picIds.isNullOrEmpty()) {
                    val picIdToUrlMap = picIds.associateWith { "${serverProperties.hostname}/api/pictures/${Domains.COLLABORATIONS}/$it" }
                    it.withPictureMap(picIdToUrlMap)
                } else {
                    it
                }
            }
        } else {
            dtos
        }

        return CollaborationListResponseDto(answer.second, result.sortedBy { it.order })
    }

    override fun get(id: Long): CollaborationResponseDto {
        val pictureId = pictureService.getLight(PictureDomainGetLightRequest.collaboration(id)).let { it.ifEmpty { null } }
        val map = pictureId?.associateWith { "${serverProperties.hostname}/api/pictures/${Domains.COLLABORATIONS}/$it" }
        return collaborationRepository.findById(id)
            .map { collaborationMapper.fromEntity(it) }
            .map { dto -> map?.let { dto.withPictureMap(it) } ?: dto }
            .orElseThrowNotFound("collaboration with id $id not found")
    }

    @Transactional
    override fun update(dto: CollaborationCreateRequestDto): CollaborationResponseDto {
        val fromDto = collaborationMapper.fromDto(dto)
        val result = if (fromDto.id == null) {
            save(fromDto, collaborationRepository.findMaxOrder() + 1)
        } else {
            val fromDB = collaborationRepository.getById(fromDto.id)
            if (fromDB.order != fromDto.order) {
                save(fromDto, fromDB.order ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Должен быть указан порядок для обновления сущности ${dto.id}").andLog())
            } else {
                collaborationRepository.save(fromDto)
            }
        }
        return collaborationMapper.fromEntity(result)
    }

    private fun save(
        fromDto: Collaboration,
        orderToExisting: Long
    ): Collaboration {
        return if (fromDto.order == null) {
            collaborationRepository.save(fromDto.apply { order = orderToExisting })
        } else {
            collaborationRepository.findByOrder(fromDto.order!!)?.let {
                it.order = orderToExisting
                collaborationRepository.saveAll(listOf(it, fromDto))
                fromDto
            } ?: collaborationRepository.save(fromDto)
        }
    }

    @Transactional
    override fun delete(ids: Collection<Long>) {
        pictureService.delete(PictureDomainDeleteRequest.collaboration(ids.associateWith { null }))
        collaborationRepository.deleteAllByIdInBatch(ids)
    }

    override fun addCollaborationsPics(id: Long, files: Collection<MultipartFile>) {
        if (!collaborationRepository.existsById(id)) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "collaboration with id $id not found").andLog()
        }
        pictureService.create(PictureDomainCreateRequest.collaboration(id, files))
    }

    override fun deleteCollaborationPics(id: Long, uuids: Collection<UUID>) {
        pictureService.delete(PictureDomainDeleteRequest.collaboration(mapOf(id to uuids)), false)
    }
}