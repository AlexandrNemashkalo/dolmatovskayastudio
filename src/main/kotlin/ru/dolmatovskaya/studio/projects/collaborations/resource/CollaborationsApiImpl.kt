package ru.dolmatovskaya.studio.projects.collaborations.resource

import java.util.UUID
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.COLLABORATIONS
import ru.dolmatovskaya.studio.consts.Endpoints.PROJECTS
import ru.dolmatovskaya.studio.projects.collaborations.api.CollaborationsApi
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationCreateRequestDto
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationListResponseDto
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationResponseDto
import ru.dolmatovskaya.studio.projects.collaborations.service.CollaborationsService
import ru.dolmatovskaya.studio.users.model.User

private val log = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$PROJECTS/$COLLABORATIONS")
@CrossOrigin("*")
class CollaborationsApiImpl(
    private val collaborationsService: CollaborationsService
) : CollaborationsApi {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    override fun create(@RequestBody dto: CollaborationCreateRequestDto, @AuthenticationPrincipal user: User): CollaborationResponseDto {
        log.info { "Create by $user $dto" }
        return collaborationsService.create(dto.apply { order = null })
    }

    @GetMapping
    override fun getAll(
        @RequestParam(required = false, defaultValue = "0") page: Int,
        @RequestParam(required = false, defaultValue = "25") size: Int
    ): CollaborationListResponseDto {
        return collaborationsService.getAll(page, size)
    }

    @GetMapping("{id}")
    override fun get(@PathVariable id: Long): CollaborationResponseDto {
        return collaborationsService.get(id)
    }

    @PutMapping("{id}")
    override fun update(@PathVariable id: Long, @RequestBody dto: CollaborationCreateRequestDto, @AuthenticationPrincipal user: User): CollaborationResponseDto {
        log.info { "Update $user $id $dto" }
        return collaborationsService.update(dto.withId(id))
    }

    @DeleteMapping
    override fun delete(@RequestParam ids: Set<Long>, @AuthenticationPrincipal user: User) {
        log.info { "Delete by $user $ids" }
        collaborationsService.delete(ids)
    }

    @PostMapping("{id}/collaboration_pictures", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    override fun addCollaborationsPics(@PathVariable id: Long, @RequestParam("files") files: List<MultipartFile>, @AuthenticationPrincipal user: User) {
        log.info { "Add pics by $user $id" }
        collaborationsService.addCollaborationsPics(id, files)
    }

    @DeleteMapping("{id}/collaboration_pictures")
    override fun deleteCollaborationPics(@PathVariable id: Long, @RequestParam uuids: Set<UUID>, @AuthenticationPrincipal user: User) {
        log.info { "Delete pics by $user $uuids" }
        collaborationsService.deleteCollaborationPics(id, uuids)
    }
}