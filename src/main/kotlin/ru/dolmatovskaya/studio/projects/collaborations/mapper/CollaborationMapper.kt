package ru.dolmatovskaya.studio.projects.collaborations.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationCreateRequestDto
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationResponseDto
import ru.dolmatovskaya.studio.projects.collaborations.model.Collaboration

@Mapper
interface CollaborationMapper {
    fun fromDto(dto: CollaborationCreateRequestDto): Collaboration = Collaboration(dto.nameRus, dto.nameEng, dto.pictureUrls, dto.externalUrl, dto.order, dto.id)
    fun fromEntity(entity: Collaboration): CollaborationResponseDto
    fun fromEntities(entities: Collection<Collaboration>): List<CollaborationResponseDto>
}