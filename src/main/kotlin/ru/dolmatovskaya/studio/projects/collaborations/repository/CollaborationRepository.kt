package ru.dolmatovskaya.studio.projects.collaborations.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.dolmatovskaya.studio.projects.collaborations.model.Collaboration

interface CollaborationRepository : JpaRepository<Collaboration, Long> {
    fun findByOrder(order: Long): Collaboration?
    @Query("select max(c.\"order\") from collaborations c", nativeQuery = true)
    fun findMaxOrder(): Long
}