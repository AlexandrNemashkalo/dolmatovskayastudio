package ru.dolmatovskaya.studio.projects.collaborations.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import java.util.UUID
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationCreateRequestDto
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationListResponseDto
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationResponseDto
import ru.dolmatovskaya.studio.users.model.User

interface CollaborationsApi {

    @Operation(
        description = "Создать запись о коллаборации",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "Created", responseCode = "201")]
    )
    fun create(@Valid @RequestBody dto: CollaborationCreateRequestDto, @Parameter(hidden = true) user: User): CollaborationResponseDto

    @Operation(
        description = "Все коллаборации",
        summary = "Для неавторизованных. Если параметр size = -1, то вернутся все сущности из БД",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun getAll(
        @PositiveOrZero @Parameter(`in` = ParameterIn.QUERY, required = false) page: Int = 0,
        @Min(-1)  @Parameter(`in` = ParameterIn.QUERY, required = false) size: Int = 25
    ): CollaborationListResponseDto

    @Operation(
        description = "Коллаборация по id",
        summary = "Для неавторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun get(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long): CollaborationResponseDto

    @Operation(
        description = "Обновить все поля по записи о коллаборации",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun update(
        @Positive @Parameter(`in` = ParameterIn.PATH) id: Long,
        @Valid @RequestBody dto: CollaborationCreateRequestDto,
        @Parameter(hidden = true) user: User
    ): CollaborationResponseDto

    @Operation(
        description = "Удалить записи о коллаборациях",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun delete(@Parameter(`in` = ParameterIn.QUERY) @NotEmpty ids: Set<Long>, @Parameter(hidden = true) user: User)


    // pictures api

    @Operation(
        description = "Добавить изображения к коллаборации",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun addCollaborationsPics(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long, files: List<MultipartFile>, @Parameter(hidden = true) user: User)

    @Operation(
        description = "Удалить изображения у коллаборации",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun deleteCollaborationPics(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long,
                                @NotEmpty @Parameter(`in` = ParameterIn.QUERY) uuids: Set<UUID>,
                                @Parameter(hidden = true) user: User)
    
}