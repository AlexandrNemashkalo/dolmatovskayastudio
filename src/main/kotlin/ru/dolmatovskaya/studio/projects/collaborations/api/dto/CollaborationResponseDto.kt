package ru.dolmatovskaya.studio.projects.collaborations.api.dto

import java.util.UUID

data class CollaborationResponseDto(
    val id: Long,
    val nameRus: String,
    val nameEng: String,
    val pictureUrls: List<String>? = null,
    val externalUrl: String? = null,
    var pictures: Map<UUID, String>? = null,
    val order: Long,
) {
    fun withPictureMap(pictures: Map<UUID, String>) = CollaborationResponseDto(id, nameRus, nameEng, pictureUrls, externalUrl, pictures, order)
}
