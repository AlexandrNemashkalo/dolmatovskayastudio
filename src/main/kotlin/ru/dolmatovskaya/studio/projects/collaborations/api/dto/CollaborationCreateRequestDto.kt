package ru.dolmatovskaya.studio.projects.collaborations.api.dto

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

data class CollaborationCreateRequestDto(
    @field:Size(max = 64)
    @field:NotBlank
    val nameRus: String,

    @field:Size(max = 64)
    @field:NotBlank
    val nameEng: String,

    val pictureUrls: List<String>? = null,

    @field:Size(max = 512)
    val externalUrl: String? = null,
    @field:PositiveOrZero
    var order: Long? = null,

    @field:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val id: Long? = null
) {
    fun withId(id: Long) = CollaborationCreateRequestDto(nameRus, nameEng, pictureUrls, externalUrl, order, id)
}