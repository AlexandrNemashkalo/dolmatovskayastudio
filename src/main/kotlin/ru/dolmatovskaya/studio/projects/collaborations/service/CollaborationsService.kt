package ru.dolmatovskaya.studio.projects.collaborations.service

import java.util.UUID
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationCreateRequestDto
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationListResponseDto
import ru.dolmatovskaya.studio.projects.collaborations.api.dto.CollaborationResponseDto

interface CollaborationsService {
    fun create(dto: CollaborationCreateRequestDto): CollaborationResponseDto
    fun getAll(page: Int, size: Int): CollaborationListResponseDto
    fun get(id: Long): CollaborationResponseDto
    fun update(dto: CollaborationCreateRequestDto): CollaborationResponseDto
    fun delete(ids: Collection<Long>)
    fun addCollaborationsPics(id: Long, files: Collection<MultipartFile>)
    fun deleteCollaborationPics(id: Long, uuids: Collection<UUID>)
}