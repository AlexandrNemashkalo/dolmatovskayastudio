package ru.dolmatovskaya.studio.projects.collaborations.api.dto

data class CollaborationListResponseDto(
    val totalCount: Long,
    val value: List<CollaborationResponseDto> = emptyList()
) {
    companion object {
        fun empty() = CollaborationListResponseDto(0)
    }
}