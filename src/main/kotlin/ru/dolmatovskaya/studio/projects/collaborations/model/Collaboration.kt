package ru.dolmatovskaya.studio.projects.collaborations.model

import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import org.hibernate.Hibernate
import ru.dolmatovskaya.studio.converter.PictureUrlsConverter

@Entity
@Table(name = "collaborations")
data class Collaboration(
    val nameRus: String,
    val nameEng: String,
    @Convert(converter = PictureUrlsConverter::class)
    val pictureUrls: List<String>? = null,
    val externalUrl: String? = null,
    @Column(name = "\"order\"")
    var order: Long? = null,
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "collaborations_id_gen")
    @SequenceGenerator(name = "collaborations_id_gen", sequenceName = "collaborations_id_seq", allocationSize = 1)
    val id: Long? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Collaboration

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , nameRus = $nameRus, pictureUrls = $pictureUrls, nameEng = $nameEng )"
    }
}