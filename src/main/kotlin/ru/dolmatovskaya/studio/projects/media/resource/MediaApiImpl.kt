package ru.dolmatovskaya.studio.projects.media.resource

import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.MEDIA
import ru.dolmatovskaya.studio.consts.Endpoints.PROJECTS
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.projects.media.api.MediaApi
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaCreateRequestDto
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaListResponseDto
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaResponseDto
import ru.dolmatovskaya.studio.projects.media.service.MediaService
import ru.dolmatovskaya.studio.users.model.User

private val log = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$PROJECTS/$MEDIA")
@CrossOrigin("*")
class MediaApiImpl(
    private val mediaService: MediaService
) : MediaApi {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    override fun create(@RequestBody dto: MediaCreateRequestDto, @AuthenticationPrincipal user: User): MediaResponseDto {
        log.info { "Create by $user $dto" }
        return mediaService.create(dto)
    }

    @GetMapping("{id}")
    override fun get(@PathVariable id: Long): Pictuable {
        return mediaService.getById(id)
    }

    @GetMapping
    override fun getAll(
        @RequestParam(required = false, defaultValue = "0") page: Int,
        @RequestParam(required = false, defaultValue = "25") size: Int
    ): MediaListResponseDto {
        return mediaService.getAll(page, size)
    }

    @PutMapping("{id}")
    override fun update(@PathVariable id: Long, @RequestBody dto: MediaCreateRequestDto, @AuthenticationPrincipal user: User): MediaResponseDto {
        log.info { "Update by $user $id $dto" }
        return mediaService.update(dto.withId(id))
    }

    @DeleteMapping
    override fun delete(@RequestParam ids: Set<Long>, @AuthenticationPrincipal user: User) {
        log.info { "Delete by $user $ids" }
        mediaService.delete(ids)
    }

    @PostMapping("{id}/preview_picture", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    override fun updatePosterPicture(@PathVariable id: Long, @RequestParam("file") file: MultipartFile, @AuthenticationPrincipal user: User) {
        log.info { "update poster $user $id" }
        mediaService.updatePosterPicture(id, file)
    }
}