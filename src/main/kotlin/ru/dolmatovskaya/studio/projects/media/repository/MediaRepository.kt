package ru.dolmatovskaya.studio.projects.media.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.dolmatovskaya.studio.projects.collaborations.model.Collaboration
import ru.dolmatovskaya.studio.projects.media.model.Media

interface MediaRepository : JpaRepository<Media, Long> {
    fun findByOrder(order: Long): Media?
    @Query("select max(c.\"order\") from media c", nativeQuery = true)
    fun findMaxOrder(): Long
}