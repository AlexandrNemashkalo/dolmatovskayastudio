package ru.dolmatovskaya.studio.projects.media.api.dto

import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable

data class MediaResponseDto(
    val id: Long,
    val mediaUrl: String? = null,
    val mediaType: String,
    override val pictureUrl: String? = null,
    val order: Long,
) : Pictuable {
    override fun withPictureUrl(url: String) = MediaResponseDto(id, mediaUrl, mediaType, url, order)
}