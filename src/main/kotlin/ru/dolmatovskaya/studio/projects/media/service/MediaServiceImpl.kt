package ru.dolmatovskaya.studio.projects.media.service

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.MEDIA
import ru.dolmatovskaya.studio.consts.Endpoints.PICTURES
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.pictures.api.PictureService
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.pictures.domain.UpdatePictureRequest
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.projects.common.single.service.CommonService
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaCreateRequestDto
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaListResponseDto
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaResponseDto
import ru.dolmatovskaya.studio.projects.media.mapper.MediaMapper
import ru.dolmatovskaya.studio.projects.media.model.Media
import ru.dolmatovskaya.studio.projects.media.repository.MediaRepository
import ru.dolmatovskaya.studio.property.ServerProperties

@Service
class MediaServiceImpl(
    private val mediaRepository: MediaRepository,
    private val mediaMapper: MediaMapper,
    private val pictureService: PictureService,
    private val serverProperties: ServerProperties,
    private val commonService: CommonService
) : MediaService {

    override fun create(dto: MediaCreateRequestDto): MediaResponseDto {
        return mediaMapper.fromEntity(mediaRepository.save(mediaMapper.fromDto(dto)))
    }

    override fun getAll(page: Int, size: Int): MediaListResponseDto {
        val sort = Sort.by(Sort.Order.asc("order"))
        val answer = if (size == -1) {
            mediaRepository.findAll(sort).let { it to it.size.toLong() }
        } else {
            val media = mediaRepository.findAll(PageRequest.of(page, size, sort))

            media.content to media.totalElements
        }
        val media = answer.first

        if (media.isEmpty()) {
            return MediaListResponseDto.empty()
        }

        val map = pictureService.getLights(media.map { PictureDomainGetLightRequest.media(it.id!!) })

        val dtos = mediaMapper.fromEntities(media)

        val result = if (map.isNotEmpty()) {
            dtos.map {
                if (it.pictureUrl != null) {
                    it
                } else {
                    val picId = map[it.id]?.firstOrNull()
                    if (picId != null) {
                        it.withPictureUrl("${serverProperties.hostname}/$API/$PICTURES/$MEDIA/${picId}")
                    } else {
                        it
                    }
                }
            }
        } else {
            dtos
        }

        return MediaListResponseDto(answer.second, result.sortedBy { it.order })
    }

    override fun getById(id: Long): Pictuable {
        return commonService.getById(mediaRepository, mediaMapper, MEDIA, id)
    }

    @Transactional
    override fun update(dto: MediaCreateRequestDto): MediaResponseDto {
        if (dto.pictureUrl != null) {
            pictureService.delete(PictureDomainDeleteRequest.media(setOf(dto.id!!)))
        }
        val fromDto = mediaMapper.fromDto(dto)
        val result = if (fromDto.id == null) {
            save(fromDto, mediaRepository.findMaxOrder() + 1)
        } else {
            val fromDB = mediaRepository.getById(fromDto.id)
            if (fromDB.order != fromDto.order) {
                save(fromDto, fromDB.order ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Должен быть указан порядок для обновления сущности ${dto.id}").andLog())
            } else {
                mediaRepository.save(fromDto)
            }
        }
        return mediaMapper.fromEntity(result)
    }

    private fun save(
        fromDto: Media,
        orderToExisting: Long
    ): Media {
        return if (fromDto.order == null) {
            mediaRepository.save(fromDto.apply { order = orderToExisting })
        } else {
            mediaRepository.findByOrder(fromDto.order!!)?.let {
                it.order = orderToExisting
                mediaRepository.saveAll(listOf(it, fromDto))
                fromDto
            } ?: mediaRepository.save(fromDto)
        }
    }

    @Transactional
    override fun delete(ids: Set<Long>) {
        pictureService.delete(PictureDomainDeleteRequest.media(ids))
        mediaRepository.deleteAllByIdInBatch(ids)
    }

    override fun updatePosterPicture(id: Long, file: MultipartFile) {
        if (!mediaRepository.existsById(id)) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "media with id $id not found").andLog()
        }
        pictureService.update(UpdatePictureRequest.media(id, file))
    }
}