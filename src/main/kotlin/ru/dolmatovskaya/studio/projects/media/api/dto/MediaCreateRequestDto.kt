package ru.dolmatovskaya.studio.projects.media.api.dto

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

data class MediaCreateRequestDto(
    @field:Size(max = 512)
    val mediaUrl: String? = null,
    @field:NotBlank
    @field:Size(max = 32)
    val mediaType: String,
    val pictureUrl: String? = null,
    @field:PositiveOrZero
    var order: Long? = null,
    @field:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val id: Long? = null
) {
    fun withId(id: Long) = MediaCreateRequestDto(mediaUrl, mediaType, pictureUrl, order, id)
}
