package ru.dolmatovskaya.studio.projects.media.api.dto

data class MediaListResponseDto(
    val totalCount: Long,
    val value: List<MediaResponseDto> = emptyList()
) {
    companion object {
        fun empty() = MediaListResponseDto(0)
    }
}
