package ru.dolmatovskaya.studio.projects.media.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.projects.common.single.mapper.IMapper
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaCreateRequestDto
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaResponseDto
import ru.dolmatovskaya.studio.projects.media.model.Media

@Mapper
interface MediaMapper : IMapper<Media> {
    fun fromDto(dto: MediaCreateRequestDto): Media = Media(dto.mediaUrl, dto.mediaType, dto.pictureUrl, dto.order, dto.id)
    override fun fromEntity(entity: Media): MediaResponseDto
    fun fromEntities(entities: Collection<Media>): List<MediaResponseDto>
}