package ru.dolmatovskaya.studio.projects.media.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import org.hibernate.Hibernate

@Entity
@Table(name = "media")
data class Media(
    val mediaUrl: String? = null,
    val mediaType: String,
    val pictureUrl: String? = null,
    @Column(name = "\"order\"")
    var order: Long? = null,
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "media_id_gen")
    @SequenceGenerator(name = "media_id_gen", sequenceName = "media_id_seq", allocationSize = 1)
    val id: Long? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Media

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id, mediaUrl = $mediaUrl, mediaType = $mediaType, pictureUrl = $pictureUrl)"
    }
}