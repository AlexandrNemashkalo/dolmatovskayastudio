package ru.dolmatovskaya.studio.projects.media.service

import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.projects.common.single.service.IService
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaCreateRequestDto
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaListResponseDto
import ru.dolmatovskaya.studio.projects.media.api.dto.MediaResponseDto

interface MediaService : IService {
    fun create(dto: MediaCreateRequestDto): MediaResponseDto
    fun getAll(page: Int, size: Int): MediaListResponseDto
    fun update(dto: MediaCreateRequestDto): MediaResponseDto
    fun delete(ids: Set<Long>)
    fun updatePosterPicture(id: Long, file: MultipartFile)
    override fun getById(id: Long): Pictuable
}