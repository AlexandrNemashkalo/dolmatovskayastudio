package ru.dolmatovskaya.studio.projects.theaters.api.dto

import java.util.UUID

data class TheaterResponseDto(
    val id: Long,
    val nameRus: String,
    val nameEng: String,
    val descriptionRus: String,
    val descriptionEng: String,
    val pictures: Map<UUID, String>? = null,
    val pictureUrls: List<String>? = null,
    val order: Long,
) {
    fun withPictureMap(pictures: Map<UUID, String>?) = TheaterResponseDto(id, nameRus, nameEng, descriptionRus, descriptionEng, pictures, pictureUrls, order)
}