package ru.dolmatovskaya.studio.projects.theaters.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import java.util.UUID
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero
import org.springframework.validation.annotation.Validated
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterCreateRequestDto
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterListResponseDto
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterResponseDto
import ru.dolmatovskaya.studio.users.model.User

@Validated
interface TheaterApi {

    @Operation(
        description = "Создать запись о спектакле",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "Created", responseCode = "201")]
    )
    fun create(@Valid @RequestBody dto: TheaterCreateRequestDto, @Parameter(hidden = true) user: User): TheaterResponseDto

    @Operation(
        description = "Спектакль по id",
        summary = "Для неавторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun get(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long): TheaterResponseDto

    @Operation(
        description = "Все спектакли",
        summary = "Для неавторизованных. Если size = -1, то вернуть все записи",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun getAll(
        @PositiveOrZero @Parameter(`in` = ParameterIn.QUERY, required = false) page: Int = 0,
        @Min(-1) @Parameter(`in` = ParameterIn.QUERY, required = false) size: Int = 25
    ): TheaterListResponseDto

    @Operation(
        description = "Обновить все поля по записи о спектакле",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun update(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long, @Valid @RequestBody dto: TheaterCreateRequestDto, @Parameter(hidden = true) user: User): TheaterResponseDto

    @Operation(
        description = "Удалить записи о спектакле",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun delete(@Parameter(`in` = ParameterIn.QUERY) @NotEmpty ids: Set<Long>, @Parameter(hidden = true) user: User)

    // picture api

    @Operation(
        description = "Добавить изображения к спектаклю",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun addTheaterPics(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long, files: List<MultipartFile>, @Parameter(hidden = true) user: User)

    @Operation(
        description = "Удалить изображения у спектакля",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun deleteTheaterPics(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long,
                          @NotEmpty @Parameter(`in` = ParameterIn.QUERY) uuids: Set<UUID>,
                          @Parameter(hidden = true) user: User)

}