package ru.dolmatovskaya.studio.projects.theaters.service

import java.util.*
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterCreateRequestDto
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterListResponseDto
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterResponseDto

interface TheaterService {
    fun getAll(page: Int, size: Int): TheaterListResponseDto
    fun create(dto: TheaterCreateRequestDto): TheaterResponseDto
    fun update(dto: TheaterCreateRequestDto): TheaterResponseDto
    fun delete(ids: Set<Long>)
    fun getById(id: Long): TheaterResponseDto
    fun addTheaterPics(id: Long, files: List<MultipartFile>)
    fun deleteTheaterPics(id: Long, uuids: Set<UUID>)
}