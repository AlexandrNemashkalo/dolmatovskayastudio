package ru.dolmatovskaya.studio.projects.theaters.api.dto

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

data class TheaterCreateRequestDto(
    @field:NotBlank
    @field:Size(max = 64)
    val nameRus: String,
    @field:NotBlank
    @field:Size(max = 64)
    val nameEng: String,
    @field:NotBlank
    val descriptionRus: String,
    @field:NotBlank
    val descriptionEng: String,
    val pictureUrls: List<String>? = null,
    @field:PositiveOrZero
    var order: Long? = null,
    @field:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val id: Long? = null,
) {
    fun withId(id: Long): TheaterCreateRequestDto = TheaterCreateRequestDto(nameRus, nameEng, descriptionRus, descriptionEng, pictureUrls, order, id)
}