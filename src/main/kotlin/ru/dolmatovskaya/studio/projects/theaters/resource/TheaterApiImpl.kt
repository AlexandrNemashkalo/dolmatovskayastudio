package ru.dolmatovskaya.studio.projects.theaters.resource

import java.util.UUID
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.PROJECTS
import ru.dolmatovskaya.studio.consts.Endpoints.THEATERS
import ru.dolmatovskaya.studio.projects.theaters.api.TheaterApi
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterCreateRequestDto
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterListResponseDto
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterResponseDto
import ru.dolmatovskaya.studio.projects.theaters.service.TheaterService
import ru.dolmatovskaya.studio.users.model.User

private val log = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$PROJECTS/$THEATERS")
@CrossOrigin("*")
class TheaterApiImpl(
    private val theaterService: TheaterService
) : TheaterApi {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    override fun create(@RequestBody dto: TheaterCreateRequestDto, @AuthenticationPrincipal user: User): TheaterResponseDto {
        log.info { "Create by $user $dto" }
        return theaterService.create(dto)
    }

    @GetMapping
    override fun getAll(
        @RequestParam(required = false, defaultValue = "0") page: Int,
        @RequestParam(required = false, defaultValue = "25") size: Int
    ): TheaterListResponseDto {
        return theaterService.getAll(page, size)
    }

    @GetMapping("{id}")
    override fun get(@PathVariable id: Long): TheaterResponseDto {
        return theaterService.getById(id)
    }

    @PutMapping("{id}")
    override fun update(@PathVariable id: Long, @RequestBody dto: TheaterCreateRequestDto, @AuthenticationPrincipal user: User): TheaterResponseDto {
        log.info { "Update $user $id $dto" }
        return theaterService.update(dto.withId(id))
    }

    @DeleteMapping
    override fun delete(@RequestParam ids: Set<Long>, @AuthenticationPrincipal user: User) {
        log.info { "Delete by $user $ids" }
        theaterService.delete(ids)
    }

    @PostMapping("{id}/theater_pictures", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    override fun addTheaterPics(@PathVariable id: Long, @RequestParam("files") files: List<MultipartFile>, @AuthenticationPrincipal user: User) {
        log.info { "Update pics by $user $id" }
        theaterService.addTheaterPics(id, files)
    }

    @DeleteMapping("{id}/theater_pictures")
    override fun deleteTheaterPics(@PathVariable id: Long, @RequestParam uuids: Set<UUID>, @AuthenticationPrincipal user: User) {
        log.info { "Delete pics $user $id $uuids" }
        theaterService.deleteTheaterPics(id, uuids)
    }
}