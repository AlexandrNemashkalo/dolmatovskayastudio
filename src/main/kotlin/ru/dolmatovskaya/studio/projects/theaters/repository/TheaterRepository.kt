package ru.dolmatovskaya.studio.projects.theaters.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.dolmatovskaya.studio.projects.collaborations.model.Collaboration
import ru.dolmatovskaya.studio.projects.theaters.model.Theater

interface TheaterRepository : JpaRepository<Theater, Long> {
    fun findByOrder(order: Long): Theater?
    @Query("select max(c.\"order\") from theaters c", nativeQuery = true)
    fun findMaxOrder(): Long
}