package ru.dolmatovskaya.studio.projects.theaters.api.dto

data class TheaterListResponseDto(
    val totalCount: Long,
    val value: List<TheaterResponseDto> = emptyList()
) {
    companion object {
        fun empty() = TheaterListResponseDto(0)
    }
}