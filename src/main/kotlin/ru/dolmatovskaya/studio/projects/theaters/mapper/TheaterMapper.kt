package ru.dolmatovskaya.studio.projects.theaters.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterCreateRequestDto
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterResponseDto
import ru.dolmatovskaya.studio.projects.theaters.model.Theater

@Mapper
interface TheaterMapper {
    fun fromDto(dto: TheaterCreateRequestDto): Theater = Theater(dto.nameRus, dto.nameEng, dto.descriptionRus, dto.descriptionEng, dto.pictureUrls, dto.order, dto.id)
    fun fromEntity(entity: Theater): TheaterResponseDto
    fun fromEntities(entities: Collection<Theater>): List<TheaterResponseDto>
}