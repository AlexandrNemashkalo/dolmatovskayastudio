package ru.dolmatovskaya.studio.projects.theaters.service

import java.util.UUID
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.consts.Domains
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.extensions.orElseThrowNotFound
import ru.dolmatovskaya.studio.pictures.api.PictureService
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainCreateRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterCreateRequestDto
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterListResponseDto
import ru.dolmatovskaya.studio.projects.theaters.api.dto.TheaterResponseDto
import ru.dolmatovskaya.studio.projects.theaters.mapper.TheaterMapper
import ru.dolmatovskaya.studio.projects.theaters.model.Theater
import ru.dolmatovskaya.studio.projects.theaters.repository.TheaterRepository
import ru.dolmatovskaya.studio.property.ServerProperties

@Service
class TheaterServiceImpl(
    private val theaterRepository: TheaterRepository,
    private val theaterMapper: TheaterMapper,
    private val pictureService: PictureService,
    private val serverProperties: ServerProperties
) : TheaterService {

    override fun create(dto: TheaterCreateRequestDto): TheaterResponseDto {
        return theaterMapper.fromEntity(theaterRepository.save(theaterMapper.fromDto(dto)))
    }

    override fun getAll(page: Int, size: Int): TheaterListResponseDto {
        val sort = Sort.by(Sort.Order.asc("order"))
        val answer = if (size == -1) {
            theaterRepository.findAll(sort).let { it to it.size.toLong() }
        } else {
            theaterRepository.findAll(PageRequest.of(page, size, sort)).let { it.content to it.totalElements }
        }

        val theaters = answer.first

        if (theaters.isEmpty()) {
            return TheaterListResponseDto.empty()
        }

        val map = pictureService.getLights(theaters.map { PictureDomainGetLightRequest.theater(it.id!!) })

        val dtos = theaterMapper.fromEntities(theaters)

        val result = if (map.isNotEmpty()) {
            dtos.map {
                val picIds = map[it.id]
                if (!picIds.isNullOrEmpty()) {
                    val picIdToUrlMap = picIds.associateWith { "${serverProperties.hostname}/api/pictures/${Domains.THEATERS}/$it" }
                    it.withPictureMap(picIdToUrlMap)
                } else {
                    it
                }
            }
        } else {
            dtos
        }

        return TheaterListResponseDto(answer.second, result.sortedBy { it.order })
    }

    override fun getById(id: Long): TheaterResponseDto {
        val pictureId = pictureService.getLight(PictureDomainGetLightRequest.theater(id)).let { it.ifEmpty { null } }
        val map = pictureId?.associateWith { "${serverProperties.hostname}/api/pictures/${Domains.THEATERS}/$it" }
        return theaterRepository.findById(id)
            .map { theaterMapper.fromEntity(it) }
            .map { dto -> map?.let { dto.withPictureMap(it) } ?: dto }
            .orElseThrowNotFound("theater with id $id not found")
    }

    override fun update(dto: TheaterCreateRequestDto): TheaterResponseDto {
        val fromDto = theaterMapper.fromDto(dto)
        val result = if (fromDto.id == null) {
            save(fromDto, theaterRepository.findMaxOrder() + 1)
        } else {
            val fromDB = theaterRepository.getById(fromDto.id)
            if (fromDB.order != fromDto.order) {
                save(fromDto, fromDB.order ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Должен быть указан порядок для обновления сущности ${dto.id}").andLog())
            } else {
                theaterRepository.save(fromDto)
            }
        }
        return theaterMapper.fromEntity(result)
    }

    private fun save(
        fromDto: Theater,
        orderToExisting: Long
    ): Theater {
        return if (fromDto.order == null) {
            theaterRepository.save(fromDto.apply { order = orderToExisting })
        } else {
            theaterRepository.findByOrder(fromDto.order!!)?.let {
                it.order = orderToExisting
                theaterRepository.saveAll(listOf(it, fromDto))
                fromDto
            } ?: theaterRepository.save(fromDto)
        }
    }

    @Transactional
    override fun delete(ids: Set<Long>) {
        pictureService.delete(PictureDomainDeleteRequest.theater(ids.associateWith { null }))
        theaterRepository.deleteAllByIdInBatch(ids)
    }

    override fun addTheaterPics(id: Long, files: List<MultipartFile>) {
        if (!theaterRepository.existsById(id)) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "theater with id $id not found").andLog()
        }
        pictureService.create(PictureDomainCreateRequest.theater(id, files))
    }

    override fun deleteTheaterPics(id: Long, uuids: Set<UUID>) {
        pictureService.delete(PictureDomainDeleteRequest.theater(mapOf(id to uuids)), false)
    }
}