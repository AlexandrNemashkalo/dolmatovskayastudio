package ru.dolmatovskaya.studio.projects.films.service

import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.common.single.service.IService
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmCreateRequestDto
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmListResponseDto
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmResponseDto

interface FilmService : IService {
    fun getAll(page: Int, size: Int): FilmListResponseDto
//    override fun getById(id: Long): Pictuable
    fun create(dto: FilmCreateRequestDto): FilmResponseDto
    fun updatePosterPicture(id: Long, file: MultipartFile)
    fun update(dto: FilmCreateRequestDto): FilmResponseDto
    fun delete(ids: Set<Long>)
}