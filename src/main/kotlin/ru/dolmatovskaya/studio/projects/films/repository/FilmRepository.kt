package ru.dolmatovskaya.studio.projects.films.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.dolmatovskaya.studio.projects.collaborations.model.Collaboration
import ru.dolmatovskaya.studio.projects.films.model.Film

interface FilmRepository : JpaRepository<Film, Long> {
    fun findByOrder(order: Long): Film?
    @Query("select max(c.\"order\") from films c", nativeQuery = true)
    fun findMaxOrder(): Long
}