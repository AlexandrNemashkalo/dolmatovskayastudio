package ru.dolmatovskaya.studio.projects.films.api.dto

import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable

data class FilmResponseDto(
    val id: Long,
    val trailerUrl: String,
    override val pictureUrl: String? = null,
    val order: Long,
) : Pictuable {
    override fun withPictureUrl(url: String) = FilmResponseDto(id, trailerUrl, url, order)
}
