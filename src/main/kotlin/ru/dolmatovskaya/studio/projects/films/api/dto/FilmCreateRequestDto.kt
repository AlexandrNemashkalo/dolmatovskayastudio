package ru.dolmatovskaya.studio.projects.films.api.dto

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

data class FilmCreateRequestDto(
    @field:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val id: Long? = null,
    @field:Size(max = 512)
    @field:NotBlank
    val trailerUrl: String,
    val pictureUrl: String? = null,
    @field:PositiveOrZero
    var order: Long? = null,
) {
    fun withId(id: Long): FilmCreateRequestDto = FilmCreateRequestDto(id, trailerUrl, pictureUrl, order)
}