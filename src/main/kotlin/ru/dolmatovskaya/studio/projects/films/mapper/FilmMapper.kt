package ru.dolmatovskaya.studio.projects.films.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.projects.common.single.mapper.IMapper
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmCreateRequestDto
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmResponseDto
import ru.dolmatovskaya.studio.projects.films.model.Film

@Mapper
interface FilmMapper : IMapper<Film> {
    fun fromDto(dto: FilmCreateRequestDto): Film = Film(dto.trailerUrl, dto.pictureUrl, dto.id, dto.order)
    override fun fromEntity(entity: Film): FilmResponseDto
    fun fromEntities(entities: Collection<Film>): List<FilmResponseDto>
}