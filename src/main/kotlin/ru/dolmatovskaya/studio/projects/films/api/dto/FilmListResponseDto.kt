package ru.dolmatovskaya.studio.projects.films.api.dto

data class FilmListResponseDto(
    val totalCount: Long,
    val value: List<FilmResponseDto> = emptyList()
) {
    companion object {
        fun empty() = FilmListResponseDto(0)
    }
}