package ru.dolmatovskaya.studio.projects.films.resource

import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.FILMS
import ru.dolmatovskaya.studio.consts.Endpoints.PROJECTS
import ru.dolmatovskaya.studio.projects.films.api.FilmsApi
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmCreateRequestDto
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmListResponseDto
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmResponseDto
import ru.dolmatovskaya.studio.projects.films.service.FilmService
import ru.dolmatovskaya.studio.users.model.User

private val log = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$PROJECTS/$FILMS")
@CrossOrigin("*")
class FilmsApiImpl(
    private val filmService: FilmService
) : FilmsApi {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    override fun create(@RequestBody dto: FilmCreateRequestDto, @AuthenticationPrincipal user: User): FilmResponseDto {
        log.info { "Create by $user $dto" }
        return filmService.create(dto)
    }

    @PostMapping("{id}/poster_picture", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    override fun updatePosterPicture(@PathVariable id: Long, @RequestParam("file") file: MultipartFile, @AuthenticationPrincipal user: User) {
        log.info { "Update pic by $user $id" }
        filmService.updatePosterPicture(id, file)
    }

    @GetMapping
    override fun getAll(
        @RequestParam(required = false, defaultValue = "0")  page: Int,
        @RequestParam(required = false, defaultValue = "25") size: Int
    ): FilmListResponseDto {
        return filmService.getAll(page, size)
    }

    @GetMapping("{id}")
    override fun get(@PathVariable id: Long) = filmService.getById(id)

    @PutMapping("{id}")
    override fun update(@PathVariable id: Long, @RequestBody dto: FilmCreateRequestDto, @AuthenticationPrincipal user: User): FilmResponseDto {
        log.info { "Update by $user $id $dto" }
        return filmService.update(dto.withId(id))
    }


    @DeleteMapping
    override fun delete(@RequestParam ids: Set<Long>, @AuthenticationPrincipal user: User) {
        log.info { "Delete by $user $ids" }
        filmService.delete(ids)
    }
}