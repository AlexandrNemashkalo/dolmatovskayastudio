package ru.dolmatovskaya.studio.projects.films.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import org.hibernate.Hibernate

@Entity
@Table(name = "films")
data class Film(
    val trailerUrl: String,
    val pictureUrl: String? = null,
    @Column(name = "\"order\"")
    var order: Long? = null,
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "film_id_gen_name")
    @SequenceGenerator(name = "film_id_gen_name", sequenceName = "films_id_seq", allocationSize = 1)
    val id: Long? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Film

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id, pictureUrls = $pictureUrl, trailerUrl = $trailerUrl )"
    }

}