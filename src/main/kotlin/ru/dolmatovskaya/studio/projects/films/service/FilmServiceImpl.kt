package ru.dolmatovskaya.studio.projects.films.service

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.FILMS
import ru.dolmatovskaya.studio.consts.Endpoints.PICTURES
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.pictures.api.PictureService
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.pictures.domain.UpdatePictureRequest
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.projects.common.single.service.CommonService
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmCreateRequestDto
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmListResponseDto
import ru.dolmatovskaya.studio.projects.films.api.dto.FilmResponseDto
import ru.dolmatovskaya.studio.projects.films.mapper.FilmMapper
import ru.dolmatovskaya.studio.projects.films.model.Film
import ru.dolmatovskaya.studio.projects.films.repository.FilmRepository
import ru.dolmatovskaya.studio.property.ServerProperties

@Service
class FilmServiceImpl(
    private val filmRepository: FilmRepository,
    private val filmMapper: FilmMapper,
    private val pictureService: PictureService,
    private val serverProperties: ServerProperties,
    private val commonService: CommonService
) : FilmService {

    override fun create(dto: FilmCreateRequestDto): FilmResponseDto {
        return filmMapper.fromEntity(filmRepository.save(filmMapper.fromDto(dto)))
    }

    override fun getAll(page: Int, size: Int): FilmListResponseDto {
        val sort = Sort.by(Sort.Order.asc("order"))
        val answer = if (size == -1) {
            filmRepository.findAll(sort).let { it to it.size.toLong() }
        } else {
            val films = filmRepository.findAll(PageRequest.of(page, size, sort))

            films.content to films.totalElements
        }
        val films = answer.first

        if (films.isEmpty()) {
            return FilmListResponseDto.empty()
        }

        val map = pictureService.getLights(films.map { PictureDomainGetLightRequest.film(it.id!!) })

        val dtos = filmMapper.fromEntities(films)

        val result = if (map.isNotEmpty()) {
            dtos.map {
                if (it.pictureUrl != null) {
                    it
                } else {
                    val picId = map[it.id]?.firstOrNull()
                    if (picId != null) {
                        it.withPictureUrl("${serverProperties.hostname}/$API/$PICTURES/$FILMS/${picId}")
                    } else {
                        it
                    }
                }
            }
        } else {
            dtos
        }

        return FilmListResponseDto(answer.second, result.sortedBy { it.order })
    }

    override fun getById(id: Long): Pictuable {
        return commonService.getById(filmRepository, filmMapper, FILMS, id)
    }

    @Transactional
    override fun update(dto: FilmCreateRequestDto): FilmResponseDto {
        if (dto.pictureUrl != null) {
            pictureService.delete(PictureDomainDeleteRequest.film(setOf(dto.id!!)))
        }
        val fromDto = filmMapper.fromDto(dto)
        val result = if (fromDto.id == null) {
            save(fromDto, filmRepository.findMaxOrder() + 1)
        } else {
            val fromDB = filmRepository.getById(fromDto.id)
            if (fromDB.order != fromDto.order) {
                save(fromDto, fromDB.order ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Должен быть указан порядок для обновления сущности ${dto.id}").andLog())
            } else {
                filmRepository.save(fromDto)
            }
        }
        return filmMapper.fromEntity(result)
    }

    private fun save(
        fromDto: Film,
        orderToExisting: Long
    ): Film {
        return if (fromDto.order == null) {
            filmRepository.save(fromDto.apply { order = orderToExisting })
        } else {
            filmRepository.findByOrder(fromDto.order!!)?.let {
                it.order = orderToExisting
                filmRepository.saveAll(listOf(it, fromDto))
                fromDto
            } ?: filmRepository.save(fromDto)
        }
    }

    @Transactional
    override fun delete(ids: Set<Long>) {
        pictureService.delete(PictureDomainDeleteRequest.film(ids))
        filmRepository.deleteAllByIdInBatch(ids)
    }

    override fun updatePosterPicture(id: Long, file: MultipartFile) {
        if (!filmRepository.existsById(id)) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "film with id $id not found").andLog()
        }
        pictureService.update(UpdatePictureRequest.film(id, file))
    }
}