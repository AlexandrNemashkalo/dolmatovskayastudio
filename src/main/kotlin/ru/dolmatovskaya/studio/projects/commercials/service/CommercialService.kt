package ru.dolmatovskaya.studio.projects.commercials.service

import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialCreateRequestDto
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialListResponseDto
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialResponseDto
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.projects.common.single.service.IService

interface CommercialService : IService {
    fun getAll(page: Int, size: Int): CommercialListResponseDto
    fun create(dto: CommercialCreateRequestDto): CommercialResponseDto
    fun update(dto: CommercialCreateRequestDto): CommercialResponseDto
    fun delete(ids: Set<Long>)
    fun updateVideoPreviewPicture(id: Long, file: MultipartFile)
    override fun getById(id: Long): Pictuable
}