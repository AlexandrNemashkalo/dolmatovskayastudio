package ru.dolmatovskaya.studio.projects.commercials.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero
import org.springframework.validation.annotation.Validated
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialCreateRequestDto
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialListResponseDto
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialResponseDto
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.projects.common.single.resource.IResource
import ru.dolmatovskaya.studio.users.model.User

@Validated
interface CommercialsApi : IResource {

    @Operation(
        description = "Создать запись о рекламном ролике",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "Created", responseCode = "201")]
    )
    fun create(@Valid @RequestBody dto: CommercialCreateRequestDto, @Parameter(hidden = true) user: User): CommercialResponseDto

    @Operation(
        description = "Рекламный ролик по id",
        summary = "Для неавторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    override fun get(@Parameter(`in` = ParameterIn.PATH) id: Long): Pictuable

    @Operation(
        description = "Все рекламные ролики",
        summary = "Для неавторизованных. Если size = -1 вернутся все ролики",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun getAll(
        @PositiveOrZero @Parameter(`in` = ParameterIn.QUERY, required = false) page: Int = 0,
        @Min(-1) @Parameter(`in` = ParameterIn.QUERY, required = false) size: Int = 25
    ): CommercialListResponseDto

    @Operation(
        description = "Обновить все поля по записи о рекламном ролике",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun update(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long, @Valid @RequestBody dto: CommercialCreateRequestDto, @Parameter(hidden = true) user: User): CommercialResponseDto

    @Operation(
        description = "Удалить записи о рекламном ролике",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun delete(@Parameter(`in` = ParameterIn.QUERY) @NotEmpty ids: Set<Long>, @Parameter(hidden = true) user: User)


    // picture api

    @Operation(
        description = "Изменить превью у рекламного ролика",
        summary = "Для авторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun updateVideoPreviewPicture(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long, file: MultipartFile, @Parameter(hidden = true) user: User)

}