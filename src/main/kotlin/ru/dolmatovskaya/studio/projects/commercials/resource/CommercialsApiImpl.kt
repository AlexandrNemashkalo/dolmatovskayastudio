package ru.dolmatovskaya.studio.projects.commercials.resource

import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.COMMERCIALS
import ru.dolmatovskaya.studio.consts.Endpoints.PROJECTS
import ru.dolmatovskaya.studio.projects.commercials.api.CommercialsApi
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialCreateRequestDto
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialListResponseDto
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialResponseDto
import ru.dolmatovskaya.studio.projects.commercials.service.CommercialService
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.users.model.User

private val log = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$PROJECTS/$COMMERCIALS")
@CrossOrigin("*")
class CommercialsApiImpl(
    private val commercialService: CommercialService
) : CommercialsApi {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    override fun create(@RequestBody dto: CommercialCreateRequestDto, @AuthenticationPrincipal user: User): CommercialResponseDto {
        log.info { "Create by $user $dto" }

        return commercialService.create(dto)
    }

    @GetMapping("{id}")
    override fun get(@PathVariable id: Long): Pictuable {
        return commercialService.getById(id)
    }

    @GetMapping
    override fun getAll(
        @RequestParam(required = false, defaultValue = "0")  page: Int,
        @RequestParam(required = false, defaultValue = "25") size: Int
    ): CommercialListResponseDto {
        return commercialService.getAll(page, size)
    }

    @PutMapping("{id}")
    override fun update(@PathVariable id: Long, @RequestBody dto: CommercialCreateRequestDto, @AuthenticationPrincipal user: User): CommercialResponseDto {
        log.info { "Update by $user $id $dto" }
        return commercialService.update(dto.withId(id))
    }

    @DeleteMapping
    override fun delete(@RequestParam ids: Set<Long>, @AuthenticationPrincipal user: User) {
        log.info { "Delete by $user $ids" }
        return commercialService.delete(ids)
    }

    @PostMapping("{id}/commercial_video_preview_picture", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    override fun updateVideoPreviewPicture(@PathVariable id: Long, @RequestParam("file") file: MultipartFile, @AuthenticationPrincipal user: User) {
        log.info { "Update by $user $id" }
        commercialService.updateVideoPreviewPicture(id, file)
    }
}