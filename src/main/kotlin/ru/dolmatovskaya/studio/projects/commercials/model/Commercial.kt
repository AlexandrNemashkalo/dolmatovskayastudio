package ru.dolmatovskaya.studio.projects.commercials.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import org.hibernate.Hibernate

@Entity
@Table(name = "commercials")
data class Commercial(
    val nameRus: String,
    val nameEng: String,
    val videoUrl: String,
    val pictureUrl: String? = null,
    @Column(name = "\"order\"")
    var order: Long? = null,
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commercials_id_gen_name")
    @SequenceGenerator(name = "commercials_id_gen_name", sequenceName = "commercials_id_seq", allocationSize = 1)
    val id: Long? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Commercial

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    override fun toString(): String {
        return "Commercial(nameRus='$nameRus', nameEng='$nameEng', videoUrl='$videoUrl', pictureUrl = $pictureUrl, id=$id)"
    }
}