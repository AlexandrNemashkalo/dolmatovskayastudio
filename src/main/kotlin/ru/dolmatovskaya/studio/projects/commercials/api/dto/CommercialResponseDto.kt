package ru.dolmatovskaya.studio.projects.commercials.api.dto

import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable

data class CommercialResponseDto(
    val nameRus: String,
    val nameEng: String,
    val videoUrl: String,
    override val pictureUrl: String? = null,
    val order: Long,
    val id: Long
) : Pictuable {
    override fun withPictureUrl(url: String) = CommercialResponseDto(nameRus, nameEng, videoUrl, url, order, id)
}