package ru.dolmatovskaya.studio.projects.commercials.api.dto

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

data class CommercialCreateRequestDto(
    @field:NotBlank
    @field:Size(max = 64)
    val nameRus: String,

    @field:NotBlank
    @field:Size(max = 64)
    val nameEng: String,

    @field:NotBlank
    @field:Size(max = 512)
    val videoUrl: String,

    val pictureUrl: String? = null,

    @field:PositiveOrZero
    var order: Long? = null,

    @field:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val id: Long? = null
) {
    fun withId(id: Long): CommercialCreateRequestDto = CommercialCreateRequestDto(nameRus, nameEng, videoUrl, pictureUrl, order, id)
}
