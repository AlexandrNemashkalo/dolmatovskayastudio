package ru.dolmatovskaya.studio.projects.commercials.service

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.COMMERCIALS
import ru.dolmatovskaya.studio.consts.Endpoints.PICTURES
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.pictures.api.PictureService
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainDeleteRequest
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.pictures.domain.UpdatePictureRequest
import ru.dolmatovskaya.studio.projects.collaborations.model.Collaboration
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialCreateRequestDto
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialListResponseDto
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialResponseDto
import ru.dolmatovskaya.studio.projects.commercials.mapper.CommercialMapper
import ru.dolmatovskaya.studio.projects.commercials.model.Commercial
import ru.dolmatovskaya.studio.projects.commercials.repository.CommercialRepository
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.projects.common.single.service.CommonService
import ru.dolmatovskaya.studio.property.ServerProperties

@Service
class CommercialServiceImpl(
    private val commercialRepository: CommercialRepository,
    private val commercialMapper: CommercialMapper,
    private val pictureService: PictureService,
    private val serverProperties: ServerProperties,
    private val commonService: CommonService
) : CommercialService {

    override fun getById(id: Long): Pictuable {
        return commonService.getById(commercialRepository, commercialMapper, COMMERCIALS, id)
    }

    override fun getAll(page: Int, size: Int): CommercialListResponseDto {
        val sort = Sort.by(Sort.Order.asc("order"))
        val answer = if (size == -1) {
            commercialRepository.findAll(sort).let { it to it.size.toLong() }
        } else {
            val comms = commercialRepository.findAll(PageRequest.of(page, size, sort))
            comms.content to comms.totalElements
        }

        val commercials = answer.first

        if (commercials.isEmpty()) {
            return CommercialListResponseDto.empty()
        }

        val map = pictureService.getLights(commercials.map { PictureDomainGetLightRequest.commercial(it.id!!) })

        val dtos = commercialMapper.fromEntities(commercials)

        val result = if (map.isNotEmpty()) {
            dtos.map {
                if (it.pictureUrl != null) {
                    it
                } else {
                    val picId = map[it.id]?.firstOrNull()
                    if (picId != null) {
                        it.withPictureUrl("${serverProperties.hostname}/$API/$PICTURES/$COMMERCIALS/${picId}")
                    } else {
                        it
                    }
                }
            }
        } else {
            dtos
        }

        return CommercialListResponseDto(answer.second, result.sortedBy { it.order })
    }

    override fun create(dto: CommercialCreateRequestDto): CommercialResponseDto {
        return commercialMapper.fromEntity(commercialRepository.save(commercialMapper.fromDto(dto)))
    }

    @Transactional
    override fun update(dto: CommercialCreateRequestDto): CommercialResponseDto {
        if (dto.pictureUrl != null) {
            pictureService.delete(PictureDomainDeleteRequest.commercial(setOf(dto.id!!)))
        }
        val fromDto = commercialMapper.fromDto(dto)
        val result = if (fromDto.id == null) {
            save(fromDto, commercialRepository.findMaxOrder() + 1)
        } else {
            val fromDB = commercialRepository.getById(fromDto.id)
            if (fromDB.order != fromDto.order) {
                save(fromDto, fromDB.order ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Должен быть указан порядок для обновления сущности ${dto.id}").andLog())
            } else {
                commercialRepository.save(fromDto)
            }
        }
        return commercialMapper.fromEntity(result)
    }

    private fun save(
        fromDto: Commercial,
        orderToExisting: Long
    ): Commercial {
        return if (fromDto.order == null) {
            commercialRepository.save(fromDto.apply { order = orderToExisting })
        } else {
            commercialRepository.findByOrder(fromDto.order!!)?.let {
                it.order = orderToExisting
                commercialRepository.saveAll(listOf(it, fromDto))
                fromDto
            } ?: commercialRepository.save(fromDto)
        }
    }

    @Transactional
    override fun delete(ids: Set<Long>) {
        pictureService.delete(PictureDomainDeleteRequest.commercial(ids))
        commercialRepository.deleteAllByIdInBatch(ids)
    }

    override fun updateVideoPreviewPicture(id: Long, file: MultipartFile) {
        if (!commercialRepository.existsById(id)) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "commercials with id $id not found").andLog()
        }
        pictureService.update(UpdatePictureRequest.commercial(id, file))
    }
}