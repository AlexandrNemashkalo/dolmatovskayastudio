package ru.dolmatovskaya.studio.projects.commercials.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialCreateRequestDto
import ru.dolmatovskaya.studio.projects.commercials.api.dto.CommercialResponseDto
import ru.dolmatovskaya.studio.projects.commercials.model.Commercial
import ru.dolmatovskaya.studio.projects.common.single.mapper.IMapper

@Mapper
interface CommercialMapper : IMapper<Commercial> {
    fun fromDto(dto: CommercialCreateRequestDto): Commercial
    override fun fromEntity(entity: Commercial): CommercialResponseDto
    fun fromEntities(entities: Collection<Commercial>): List<CommercialResponseDto>
}