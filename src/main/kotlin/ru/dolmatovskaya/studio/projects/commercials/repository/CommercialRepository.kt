package ru.dolmatovskaya.studio.projects.commercials.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.dolmatovskaya.studio.projects.commercials.model.Commercial

interface CommercialRepository : JpaRepository<Commercial, Long> {
    fun findByOrder(order: Long): Commercial?
    @Query("select max(c.\"order\") from commercials c", nativeQuery = true)
    fun findMaxOrder(): Long
}