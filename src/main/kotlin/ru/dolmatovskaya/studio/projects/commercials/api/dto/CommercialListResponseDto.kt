package ru.dolmatovskaya.studio.projects.commercials.api.dto

data class CommercialListResponseDto(
    val totalCount: Long,
    val value: List<CommercialResponseDto> = emptyList()
) {
    companion object {
        fun empty() = CommercialListResponseDto(0)
    }
}