package ru.dolmatovskaya.studio.projects.common.single.resource

import javax.validation.constraints.Positive
import org.springframework.validation.annotation.Validated
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable

@Validated
interface IResource {
    fun get(@Positive id: Long): Pictuable
}