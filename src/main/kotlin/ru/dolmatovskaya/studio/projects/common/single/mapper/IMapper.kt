package ru.dolmatovskaya.studio.projects.common.single.mapper

import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable

interface IMapper<FROM> {
    fun fromEntity(entity: FROM): Pictuable
}