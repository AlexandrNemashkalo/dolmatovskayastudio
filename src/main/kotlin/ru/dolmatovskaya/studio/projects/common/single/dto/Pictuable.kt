package ru.dolmatovskaya.studio.projects.common.single.dto

interface Pictuable {
    val pictureUrl: String?
    fun withPictureUrl(url: String): Pictuable
}