package ru.dolmatovskaya.studio.projects.common.single.service

import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable

interface IService {
    fun getById(id: Long): Pictuable
}