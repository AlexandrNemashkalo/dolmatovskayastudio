package ru.dolmatovskaya.studio.projects.common.single.service

import org.springframework.data.jpa.repository.JpaRepository
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.projects.common.single.mapper.IMapper

interface CommonService {
    fun <FROM> getById(repo: JpaRepository<FROM, Long>, mapper: IMapper<FROM>, domainAddInfo: String, id: Long): Pictuable
}