package ru.dolmatovskaya.studio.projects.common.single.service

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Service
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.PICTURES
import ru.dolmatovskaya.studio.extensions.orElseThrowNotFound
import ru.dolmatovskaya.studio.pictures.api.PictureService
import ru.dolmatovskaya.studio.pictures.domain.PictureDomainGetLightRequest
import ru.dolmatovskaya.studio.projects.common.single.dto.Pictuable
import ru.dolmatovskaya.studio.projects.common.single.mapper.IMapper
import ru.dolmatovskaya.studio.property.ServerProperties

@Service
class CommonServiceImpl(
    private val pictureService: PictureService,
    private val serverProperties: ServerProperties
): CommonService {
    override fun <FROM> getById(repo: JpaRepository<FROM, Long>, mapper: IMapper<FROM>, domainAddInfo: String, id: Long): Pictuable {
        return repo.findById(id)
            .map { mapper.fromEntity(it) }
            .map {
                if (it.pictureUrl != null) {
                    it
                } else {
                    val picId = pictureService.getLight(PictureDomainGetLightRequest(id, domainAddInfo)).firstOrNull()
                    val url = picId?.let { "${serverProperties.hostname}/$API/$PICTURES/${domainAddInfo}/$it" }
                    url?.let { u -> it.withPictureUrl(u) } ?: it
                }
            }
            .orElseThrowNotFound("$domainAddInfo with id $id not found")
    }
}