package ru.dolmatovskaya.studio.property

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.NestedConfigurationProperty

@ConstructorBinding
@ConfigurationProperties("s3")
data class S3Properties(
    @NotBlank
    val basePath: String,
    @NotEmpty
    val domainMap: Map<String, String>,
    @NotEmpty
    val bucketName: String,
    @NotBlank
    val keyId: String,
    @NotBlank
    val secretKey: String,
    @NestedConfigurationProperty
    val canonicalRequest: CanonicalRequest = CanonicalRequest(),
    @NestedConfigurationProperty
    val stringToSign: StringToSign = StringToSign(),
    @NestedConfigurationProperty
    val signature: Signature = Signature()
) : SelfLoggable

data class CanonicalRequest(
    @NotBlank
    val host: String = "storage.yandexcloud.net",
    @NotBlank
    val s3DateTimePattern: String = "YYYYMMdd'T'HHmmss"
)

data class StringToSign(
    @NotBlank
    val prefix: String = "AWS4-HMAC-SHA256"
)

data class Signature(
    @NotBlank
    val macInstanceName: String = "HmacSHA256",
    @NotBlank
    val prefix: String = "AWS4",

)