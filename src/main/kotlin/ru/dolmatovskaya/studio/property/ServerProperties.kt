package ru.dolmatovskaya.studio.property

import javax.validation.constraints.NotBlank
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.validation.annotation.Validated

@ConstructorBinding
@ConfigurationProperties("server.properties")
@Validated
data class ServerProperties(
    @NotBlank
    val hostname: String
) : SelfLoggable