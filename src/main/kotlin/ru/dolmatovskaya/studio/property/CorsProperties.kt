package ru.dolmatovskaya.studio.property

import javax.validation.constraints.NotEmpty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("cors")
data class CorsProperties(
    @field:NotEmpty
    val config: String
) : SelfLoggable