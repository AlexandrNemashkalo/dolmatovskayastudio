package ru.dolmatovskaya.studio.property

import javax.validation.constraints.NotEmpty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.validation.annotation.Validated

@ConstructorBinding
@ConfigurationProperties("telegram.notification")
@Validated
data class TelegramProperties(
    @field:NotEmpty
    val url: String,
    @field:NotEmpty
    val methodName: String,
    @field:NotEmpty
    val token: String,
    @field:NotEmpty
    val messageTemplate: String = "",
    @field:NotEmpty
    val chatId: String
) : SelfLoggable {
    val baseUrl
        get() = "$url/bot$token/"
}