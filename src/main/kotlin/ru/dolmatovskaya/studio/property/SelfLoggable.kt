package ru.dolmatovskaya.studio.property

import javax.annotation.PostConstruct
import mu.KotlinLogging

private val logger = KotlinLogging.logger { }

interface SelfLoggable {
    @PostConstruct
    fun post() {
        logger.debug { this }
    }
}