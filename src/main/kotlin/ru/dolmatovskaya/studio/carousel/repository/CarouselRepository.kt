package ru.dolmatovskaya.studio.carousel.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.dolmatovskaya.studio.carousel.model.Carousel

interface CarouselRepository : JpaRepository<Carousel, Int>