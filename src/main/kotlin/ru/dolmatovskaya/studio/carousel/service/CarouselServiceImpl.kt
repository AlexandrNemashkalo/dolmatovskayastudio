package ru.dolmatovskaya.studio.carousel.service

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.carousel.api.dto.CarouselDto
import ru.dolmatovskaya.studio.carousel.api.dto.CarouselIdWrapper
import ru.dolmatovskaya.studio.carousel.api.dto.CarouselListResponseDto
import ru.dolmatovskaya.studio.carousel.model.Carousel
import ru.dolmatovskaya.studio.carousel.repository.CarouselRepository

@Service
class CarouselServiceImpl(
    val carouselRepository: CarouselRepository
) : CarouselService {
    override fun update(dto: CarouselDto) {
        carouselRepository.save(Carousel(dto.pictureUrls))
    }

    override fun get(): CarouselListResponseDto {
        return carouselRepository.findById(1)
            .map { CarouselListResponseDto(it.pictureUrls.size.toLong(), it.pictureUrls.map(::CarouselIdWrapper)) }
            .orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
    }
}