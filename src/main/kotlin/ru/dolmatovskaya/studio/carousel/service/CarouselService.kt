package ru.dolmatovskaya.studio.carousel.service

import ru.dolmatovskaya.studio.carousel.api.dto.CarouselDto
import ru.dolmatovskaya.studio.carousel.api.dto.CarouselListResponseDto

interface CarouselService {
    fun update(dto: CarouselDto)
    fun get(): CarouselListResponseDto
}