package ru.dolmatovskaya.studio.carousel.api

import io.swagger.v3.oas.annotations.parameters.RequestBody
import javax.validation.Valid
import ru.dolmatovskaya.studio.carousel.api.dto.CarouselDto
import ru.dolmatovskaya.studio.carousel.api.dto.CarouselListResponseDto

interface CarouselApi {
    fun get(): CarouselListResponseDto
    fun post(@Valid @RequestBody dto: CarouselDto)
}