package ru.dolmatovskaya.studio.carousel.api.dto

import kotlin.random.Random

data class CarouselIdWrapper(
    val id: Long = Random.nextLong(0, Long.MAX_VALUE),
    val pictureUrl: String,
) {
    constructor(pictureUrl: String): this(Random.nextLong(0, Long.MAX_VALUE), pictureUrl)
}