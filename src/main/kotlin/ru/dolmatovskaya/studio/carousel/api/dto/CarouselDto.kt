package ru.dolmatovskaya.studio.carousel.api.dto

import javax.validation.constraints.NotEmpty

data class CarouselDto(
    @field:NotEmpty
    val pictureUrls: List<String>
)
