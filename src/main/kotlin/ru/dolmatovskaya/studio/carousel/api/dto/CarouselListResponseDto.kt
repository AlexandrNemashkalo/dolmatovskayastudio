package ru.dolmatovskaya.studio.carousel.api.dto

data class CarouselListResponseDto(
    val totalCount: Long = 0,
    val value: List<CarouselIdWrapper> = emptyList()
)