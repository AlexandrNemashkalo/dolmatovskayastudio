package ru.dolmatovskaya.studio.carousel.resource

import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.dolmatovskaya.studio.carousel.api.CarouselApi
import ru.dolmatovskaya.studio.carousel.api.dto.CarouselDto
import ru.dolmatovskaya.studio.carousel.api.dto.CarouselListResponseDto
import ru.dolmatovskaya.studio.carousel.service.CarouselService
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.CAROUSEL

@RestController
@RequestMapping("$API/$CAROUSEL")
@CrossOrigin("*")
@Validated
class CarouselApiImpl(
    val carouselService: CarouselService
) : CarouselApi {

    @GetMapping
    override fun get(): CarouselListResponseDto = carouselService.get()


    @PostMapping
    override fun post(@RequestBody dto: CarouselDto) = carouselService.update(dto)
}