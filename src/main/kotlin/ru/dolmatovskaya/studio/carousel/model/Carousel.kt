package ru.dolmatovskaya.studio.carousel.model

import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import ru.dolmatovskaya.studio.converter.PictureUrlsConverter

@Entity
@Table(name = "carousel")
data class Carousel(
    @Convert(converter = PictureUrlsConverter::class)
    val pictureUrls: List<String>,
    @Id
    val id: Int = 1
)