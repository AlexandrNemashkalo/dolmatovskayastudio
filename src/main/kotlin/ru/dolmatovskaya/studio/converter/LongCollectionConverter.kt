package ru.dolmatovskaya.studio.converter

import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
class LongCollectionConverter : AttributeConverter<Collection<Long>, String> {
    override fun convertToDatabaseColumn(attribute: Collection<Long>): String {
        return attribute.joinToString(separator = ",")
    }

    override fun convertToEntityAttribute(dbData: String): Collection<Long> {
        return dbData.split(",").map { it.toLong() }.toSet()
    }

}