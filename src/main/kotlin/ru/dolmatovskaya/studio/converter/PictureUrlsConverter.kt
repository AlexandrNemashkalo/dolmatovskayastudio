package ru.dolmatovskaya.studio.converter

import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
class PictureUrlsConverter : AttributeConverter<List<String>, String> {
    override fun convertToDatabaseColumn(attribute: List<String>?): String? {
        return attribute?.joinToString(separator = "\n")
    }

    override fun convertToEntityAttribute(dbData: String?): List<String>? {
        return dbData?.split("\n")
    }
}