package ru.dolmatovskaya.studio.converter

import javax.persistence.AttributeConverter
import javax.persistence.Converter
import org.springframework.stereotype.Component

@Component
@Converter
class RolesConverter : AttributeConverter<Set<String>, String> {
    override fun convertToDatabaseColumn(attribute: Set<String>): String {
        return attribute.joinToString(separator = ",")
    }

    override fun convertToEntityAttribute(dbData: String): Set<String> {
        return dbData.split(",").toSet()
    }
}