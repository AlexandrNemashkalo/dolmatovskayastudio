package ru.dolmatovskaya.studio.handler

import javax.validation.ConstraintViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import ru.dolmatovskaya.studio.exceptions.S3Exception

@RestControllerAdvice
class ResponseExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(ConstraintViolationException::class)
    fun handle(e: ConstraintViolationException): ResponseEntity<ApiErrorMessage> {
        return ResponseEntity(ApiErrorMessage(e.message!!), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(S3Exception::class)
    fun handle(e: S3Exception): ResponseEntity<ApiErrorMessage> {
        return ResponseEntity(ApiErrorMessage("RAW STATUS ${e.code} RAW BODY ${e.body}"), HttpStatus.INTERNAL_SERVER_ERROR)
    }
}

data class ApiErrorMessage(
    val message: String
)