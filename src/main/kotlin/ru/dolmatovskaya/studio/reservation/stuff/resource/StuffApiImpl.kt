package ru.dolmatovskaya.studio.reservation.stuff.resource

import mu.KotlinLogging
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.FILTERS
import ru.dolmatovskaya.studio.consts.Endpoints.RESERVATION
import ru.dolmatovskaya.studio.consts.Endpoints.STUFF
import ru.dolmatovskaya.studio.reservation.stuff.api.StuffApi
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffCreateRequestDto
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffListResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.model.Sex
import ru.dolmatovskaya.studio.reservation.stuff.service.StuffService
import ru.dolmatovskaya.studio.users.model.User

private val log = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$RESERVATION/$STUFF")
@CrossOrigin("*")
@Validated
class StuffApiImpl(
    private val stuffService: StuffService
) : StuffApi {

    @PostMapping
    override fun create(@RequestBody dto: StuffCreateRequestDto, @AuthenticationPrincipal user: User): StuffResponseDto {
        log.info { "user $user is creating $dto" }
        return stuffService.create(dto)
    }

    @GetMapping
    override fun getAll(
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) size: Int?,
        @RequestParam(required = false) ids: Collection<Long>?
    ): StuffListResponseDto {
        return stuffService.getAll(page ?: 0, size ?: 25, ids)
    }

    @GetMapping("{id}")
    override fun get(@PathVariable id: Long): StuffResponseDto {
        return stuffService.get(id)
    }

    @GetMapping(FILTERS)
    override fun get(
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) size: Int?,
        @RequestParam(required = false) sizes: List<String>?,
        @RequestParam(required = false) decades: List<String>?,
        @RequestParam(required = false) sex: List<Sex>?,
        @RequestParam(required = false) categories: List<String>?,
        @RequestParam(required = false) subCategories0: List<String>?,
        @RequestParam(required = false) subCategories: List<String>?
    ): StuffListResponseDto {
        return stuffService.getAllByFilter(page ?: 0, size ?: 25, sizes, decades, sex, categories, subCategories0, subCategories)
    }

    @PutMapping("{id}")
    override fun update(@PathVariable id: Long, @RequestBody dto: StuffCreateRequestDto, @AuthenticationPrincipal user: User): StuffResponseDto {
        log.info { "user $user $id $dto" }
        return stuffService.update(dto.withId(id))
    }

    @DeleteMapping
    override fun delete(@RequestParam ids: Set<Long>, @AuthenticationPrincipal user: User) {
        log.info { "delete by user $user $ids" }
        stuffService.delete(ids)
    }
}