package ru.dolmatovskaya.studio.reservation.stuff.mapper

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffCreateRequestDto
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.model.Stuff

@Mapper
interface StuffMapper {
    fun fromDto(dto: StuffCreateRequestDto): Stuff = Stuff(dto.name, dto.description, dto.size.uppercase(), dto.decades?.uppercase(), dto.price, dto.sex, dto.category.uppercase(), dto.subCategory0?.uppercase(), dto.subCategory?.uppercase(), dto.pictureUrls, false, dto.id)
    @Mapping(target = "isBooked", source = "booked")
    fun fromEntity(entity: Stuff): StuffResponseDto
    fun fromEntities(entities: Collection<Stuff>): List<StuffResponseDto>
}