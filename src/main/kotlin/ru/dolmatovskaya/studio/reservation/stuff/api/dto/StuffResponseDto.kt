package ru.dolmatovskaya.studio.reservation.stuff.api.dto

import ru.dolmatovskaya.studio.reservation.stuff.model.Sex

data class StuffResponseDto(
    val name: String,
    val description: String? = null,
    val size: String,
    val decades: String? = null,
    val price: Int,
    val sex: Sex,
    val category: String,
    val subCategory0: String?,
    val subCategory: String?,
    val pictureUrls: List<String>? = null,
    val isBooked: Boolean,
    val id: Long? = null,
)