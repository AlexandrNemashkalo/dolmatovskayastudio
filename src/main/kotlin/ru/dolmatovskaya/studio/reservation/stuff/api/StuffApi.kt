package ru.dolmatovskaya.studio.reservation.stuff.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffCreateRequestDto
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffListResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.model.Sex
import ru.dolmatovskaya.studio.users.model.User

interface StuffApi {
    @Operation(
        description = "Создать запись о вещи под аренду",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "Created", responseCode = "201")]
    )
    fun create(@Valid @RequestBody dto: StuffCreateRequestDto, @Parameter(hidden = true) user: User): StuffResponseDto

    @Operation(
        description = "Все вещи под аренду",
        summary = "Для неавторизованных. Если параметр size = -1, то вернутся все сущности из БД",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun getAll(
        @PositiveOrZero @Parameter(`in` = ParameterIn.QUERY, required = false) page: Int? = 0,
        @Min(-1)  @Parameter(`in` = ParameterIn.QUERY, required = false) size: Int? = 25,
        @Parameter(`in` = ParameterIn.QUERY, required = false) ids: Collection<Long>? = null
    ): StuffListResponseDto

    @Operation(
        description = "Вещь под аренду по id",
        summary = "Для неавторизованных",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "NOT_FOUND", responseCode = "404")
        ]
    )
    fun get(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long): StuffResponseDto

    @Operation(description = "Отфильтровать вещи", summary = "Для неавторизованных")
    fun get(
            @Parameter(`in` = ParameterIn.QUERY, required = false) page: Int? = null,
            @Parameter(`in` = ParameterIn.QUERY, required = false) size: Int? = null,
            @Parameter(`in` = ParameterIn.QUERY, required = false) sizes: List<String>? = null,
            @Parameter(`in` = ParameterIn.QUERY, required = false) decades: List<String>? = null,
            @Parameter(`in` = ParameterIn.QUERY, required = false) sex: List<Sex>? = null,
            @Parameter(`in` = ParameterIn.QUERY, required = false) categories: List<String>? = null,
            @Parameter(`in` = ParameterIn.QUERY, required = false) subCategories0: List<String>? = null,
            @Parameter(`in` = ParameterIn.QUERY, required = false) subCategories: List<String>? = null,
    ): StuffListResponseDto

    @Operation(
        description = "Обновить все поля по записи о вещи под аренду",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun update(
        @Positive @Parameter(`in` = ParameterIn.PATH) id: Long,
        @Valid @RequestBody dto: StuffCreateRequestDto,
        @Parameter(hidden = true) user: User
    ): StuffResponseDto

    @Operation(
        description = "Удалить записи о вещах под аренду",
        summary = "Для авторизованных",
        responses = [ApiResponse(description = "OK", responseCode = "200")]
    )
    fun delete(@Parameter(`in` = ParameterIn.QUERY) @NotEmpty ids: Set<Long>, @Parameter(hidden = true) user: User)


}