package ru.dolmatovskaya.studio.reservation.stuff.model

enum class Sex {
    M, F, U
}