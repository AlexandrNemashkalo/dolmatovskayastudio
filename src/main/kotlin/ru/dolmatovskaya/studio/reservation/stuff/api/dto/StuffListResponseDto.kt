package ru.dolmatovskaya.studio.reservation.stuff.api.dto

data class StuffListResponseDto(
    val totalCount: Long,
    val value: List<StuffResponseDto> = emptyList()
) {
    companion object {
        fun empty() = StuffListResponseDto(0)
    }

    constructor(value: List<StuffResponseDto>): this(value.size.toLong(), value)
}
