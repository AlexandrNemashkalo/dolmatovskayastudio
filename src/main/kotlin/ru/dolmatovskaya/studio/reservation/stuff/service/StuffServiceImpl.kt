package ru.dolmatovskaya.studio.reservation.stuff.service

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.extensions.orElseThrowNotFound
import ru.dolmatovskaya.studio.reservation.filter.mapper.FilterMapper
import ru.dolmatovskaya.studio.reservation.filter.service.FilterService
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffCreateRequestDto
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffListResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.mapper.StuffMapper
import ru.dolmatovskaya.studio.reservation.stuff.model.Sex
import ru.dolmatovskaya.studio.reservation.stuff.repository.StuffRepository

@Service
class StuffServiceImpl(
    private val stuffMapper: StuffMapper,
    private val stuffRepository: StuffRepository,
    private val filterService: FilterService,
    private val filterMapper: FilterMapper
) : StuffService {

    @Transactional
    override fun create(dto: StuffCreateRequestDto): StuffResponseDto {
        return stuffMapper.fromEntity(stuffRepository.save(stuffMapper.fromDto(dto)))
    }

    override fun getAll(page: Int, size: Int, ids: Collection<Long>?): StuffListResponseDto {
        val sort = Sort.by(Sort.Order.asc("id"))
        val answer =
            if (ids != null) {
                val r = stuffRepository.findAllById(ids)
                r.sortBy { it.id }
                r to r.size.toLong()
            } else if (size == -1) {
                stuffRepository.findAll(sort).let { it to it.size.toLong() }
            } else {
                stuffRepository.findAll(PageRequest.of(page, size, sort)).let { it.content to it.totalElements }
            }

        val stuff = answer.first

        if (stuff.isEmpty()) {
            return StuffListResponseDto.empty()
        }

        val dtos = stuffMapper.fromEntities(stuff)

        return StuffListResponseDto(answer.second, dtos)
    }

    override fun get(id: Long): StuffResponseDto {
        return stuffRepository.findById(id)
            .map { stuffMapper.fromEntity(it) }
            .orElseThrowNotFound("stuff with id $id not found")
    }

    override fun update(dto: StuffCreateRequestDto): StuffResponseDto {
        return stuffMapper.fromEntity(stuffRepository.save(stuffMapper.fromDto(dto)))
    }

    override fun delete(ids: Collection<Long>) {
        val bookedStuff = stuffRepository.findAllByIdInAndIsBookedTrue(ids)
        if (bookedStuff.isNotEmpty()) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Попытка удалить зарезервированные вещи").andLog()
        } else {
            stuffRepository.deleteAllByIdInBatch(ids)
        }
    }

    override fun book(ids: Collection<Long>) {
        val allById = stuffRepository.findAllById(ids)
        if (allById.size != ids.size) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Количество под резервацию не совпадает с количеством вещей в бд").andLog()
        }
        for (stuff in allById) {
            stuff.isBooked = true
        }
        stuffRepository.saveAll(allById)
    }

    override fun unbook(ids: MutableSet<Long>) {
        val allById = stuffRepository.findAllById(ids)
        if (allById.size != ids.size) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Количество под освобождение от резервации не совпадает с количеством вещей в бд").andLog()
        }
        for (stuff in allById) {
            stuff.isBooked = false
        }
        stuffRepository.saveAll(allById)
    }

    @Transactional
    override fun free(ids: Collection<Long>) {
        stuffRepository.unbookAll(ids)
    }

    override fun filterBookedIfAny(stuffIds: Collection<Long>): Collection<Long> {
        return stuffRepository.findAllByIdInAndIsBookedTrue(stuffIds).map { it.id!! }
    }

    override fun getAllByFilter(
        page: Int,
        size: Int,
        sizes: List<String>?,
        decades: List<String>?,
        sex: List<Sex>?,
        categories: List<String>?,
        subCategories0: List<String>?,
        subCategories: List<String>?
    ): StuffListResponseDto {
        return filterService.getAllByFilter(page, size, filterMapper.fromQueries(sizes, decades, sex, categories, subCategories0, subCategories))
    }
}