package ru.dolmatovskaya.studio.reservation.stuff.model

import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import org.hibernate.Hibernate
import ru.dolmatovskaya.studio.converter.PictureUrlsConverter

@Entity
@Table(name = "stuff")
data class Stuff(
    val name: String,
    val description: String? = null,
    val size: String,
    val decades: String? = null,
    val price: Int,
    @Enumerated(EnumType.STRING)
    val sex: Sex,
    val category: String,
    @Column(name = "sub_category0")
    val subCategory0: String?,
    val subCategory: String?,
    @Convert(converter = PictureUrlsConverter::class)
    val pictureUrls: List<String>? = null,
    var isBooked: Boolean = false,
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stuff_id_seq_gen")
    @SequenceGenerator(name = "stuff_id_seq_gen", sequenceName = "stuff_id_seq", allocationSize = 1)
    val id: Long? = null,
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Stuff

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()
    override fun toString(): String {
        return "Stuff(name='$name', description='$description', size='$size', decades='$decades', price=$price, sex=$sex, category='$category', subCategory0=$subCategory0, subCategory='$subCategory', pictureUrls=$pictureUrls, isBooked=$isBooked, id=$id)"
    }

}