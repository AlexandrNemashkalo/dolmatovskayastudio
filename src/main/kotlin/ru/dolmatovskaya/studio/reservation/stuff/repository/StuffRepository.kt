package ru.dolmatovskaya.studio.reservation.stuff.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import ru.dolmatovskaya.studio.reservation.stuff.model.Stuff

interface StuffRepository : JpaRepository<Stuff, Long>, JpaSpecificationExecutor<Stuff> {
    fun findAllByIdInAndIsBookedTrue(ids: Collection<Long>): Collection<Stuff>
    @Modifying
    @Query("update Stuff set isBooked = false where id in :ids")
    fun unbookAll(@Param("ids") ids: Collection<Long>)
}