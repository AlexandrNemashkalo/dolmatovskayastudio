package ru.dolmatovskaya.studio.reservation.stuff.service

import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffCreateRequestDto
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffListResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.model.Sex

interface StuffService {
    fun create(dto: StuffCreateRequestDto): StuffResponseDto
    fun getAll(page: Int, size: Int, ids: Collection<Long>?): StuffListResponseDto
    fun get(id: Long): StuffResponseDto
    fun update(dto: StuffCreateRequestDto): StuffResponseDto
    fun delete(ids: Collection<Long>)
    fun book(ids: Collection<Long>)
    fun unbook(ids: MutableSet<Long>)
    fun free(ids: Collection<Long>)
    fun filterBookedIfAny(stuffIds: Collection<Long>): Collection<Long>
    fun getAllByFilter(
        page: Int,
        size: Int,
        sizes: List<String>?,
        decades: List<String>?,
        sex: List<Sex>?,
        categories: List<String>?,
        subCategories0: List<String>?,
        subCategories: List<String>?
    ): StuffListResponseDto
}