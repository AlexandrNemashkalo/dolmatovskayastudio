package ru.dolmatovskaya.studio.reservation.stuff.api.dto

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive
import javax.validation.constraints.Size
import ru.dolmatovskaya.studio.reservation.stuff.model.Sex

data class StuffCreateRequestDto(
    @field:NotBlank
    @field:Size(max = 64)
    val name: String,
    @field:Size(max = 1024)
    val description: String? = null,
    @field:NotBlank
    @field:Size(max = 8)
    val size: String,
    @field:Size(max = 32)
    val decades: String? = null,
    @field:Positive
    val price: Int,
    val sex: Sex,
    val category: String,
    @field:Size(max = 64)
    val subCategory0: String?,
    @field:Size(max = 64)
    val subCategory: String?,
    val pictureUrls: List<String>? = null,
    @field:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val id: Long? = null,
) {
    fun withId(id: Long) = StuffCreateRequestDto(name, description, size, decades, price, sex, category, subCategory0, subCategory, pictureUrls, id)
}
