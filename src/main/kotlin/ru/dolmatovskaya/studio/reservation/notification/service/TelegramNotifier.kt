package ru.dolmatovskaya.studio.reservation.notification.service

import java.time.Duration
import mu.KotlinLogging
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.WebClientException
import org.springframework.web.reactive.function.client.bodyToMono
import ru.dolmatovskaya.studio.property.TelegramProperties
import ru.dolmatovskaya.studio.reservation.notification.dto.NotificationDto

private val logger = KotlinLogging.logger { }

@Service
class TelegramNotifier(
    val telegramProperties: TelegramProperties,
    val telegramWebClient: WebClient
) : Notifier {

    override fun notifyAdmin(dto: NotificationDto) {
        try {
            val message = "Клиент ${dto.name} : ${dto.phone} совершил заказ # ${dto.bookingId}"
            logger.info { "Notifying w/ '$message'" }
            telegramWebClient.post()
                .uri { it.path(telegramProperties.methodName)
                    .queryParam("chat_id", telegramProperties.chatId)
                    .queryParam("text", message)
                    .build() }
                .retrieve()
                .bodyToMono<String>()
                .blockOptional(Duration.ofSeconds(30))
        } catch (e: WebClientException) {
            logger.warn(e) { "ERROR STATUS CODE" }
        } catch (e: Exception) {
            logger.error(e) { "FATAL NOTIFICATION" }
        }
    }
}