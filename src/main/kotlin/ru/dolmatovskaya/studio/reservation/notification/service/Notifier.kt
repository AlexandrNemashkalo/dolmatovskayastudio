package ru.dolmatovskaya.studio.reservation.notification.service

import ru.dolmatovskaya.studio.reservation.notification.dto.NotificationDto

interface Notifier {
    fun notifyAdmin(dto: NotificationDto)
}