package ru.dolmatovskaya.studio.reservation.notification.dto

data class NotificationDto(
    val bookingId: Long,
    val phone: String,
    val name: String,
)
