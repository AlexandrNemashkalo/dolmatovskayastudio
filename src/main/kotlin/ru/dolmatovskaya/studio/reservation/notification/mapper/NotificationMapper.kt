package ru.dolmatovskaya.studio.reservation.notification.mapper

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingResponseDto
import ru.dolmatovskaya.studio.reservation.notification.dto.NotificationDto

@Mapper
interface NotificationMapper {
    @Mapping(target = "bookingId", source = "id")
    fun fromBookingResponseDto(booking: BookingResponseDto): NotificationDto
}