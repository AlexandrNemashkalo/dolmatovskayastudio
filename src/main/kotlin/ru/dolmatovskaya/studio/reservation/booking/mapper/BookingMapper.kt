package ru.dolmatovskaya.studio.reservation.booking.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.extensions.normalizePhone
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingCreateRequestDto
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingResponseDto
import ru.dolmatovskaya.studio.reservation.booking.model.Booking
import ru.dolmatovskaya.studio.reservation.booking.model.BookingStatus

@Mapper
interface BookingMapper {
    fun fromDto(dto: BookingCreateRequestDto): Booking = Booking(dto.name, dto.phone.normalizePhone(), dto.monthOrDays, dto.quantity, dto.deliveryType, dto.paymentType, BookingStatus.PRE_BOOKED, dto.stuffIds.toMutableSet())
    fun fromEntity(entity: Booking): BookingResponseDto
    fun fromEntities(entity: Collection<Booking>): List<BookingResponseDto>
}