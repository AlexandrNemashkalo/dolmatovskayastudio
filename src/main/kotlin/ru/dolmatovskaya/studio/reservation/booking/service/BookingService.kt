package ru.dolmatovskaya.studio.reservation.booking.service

import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingCreateRequestDto
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingListResponseDto
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingResponseDto

interface BookingService {
    fun book(dto: BookingCreateRequestDto): BookingResponseDto
    fun getBookings(
        page: Int,
        size: Int,
        ids: Collection<Long>?,
        phone: String?,
        name: String?,
        includeDone: Boolean?
    ): BookingListResponseDto

    fun approve(id: Long)
    fun finish(id: Long)
    fun delete(ids: Collection<Long>)
}