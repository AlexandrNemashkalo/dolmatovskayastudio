package ru.dolmatovskaya.studio.reservation.booking.api.dto

data class BookingListResponseDto (
    val totalCount: Long = 0,
    val value: List<BookingResponseDto> = emptyList()
)