package ru.dolmatovskaya.studio.reservation.booking.resource

import mu.KotlinLogging
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.APPROVE
import ru.dolmatovskaya.studio.consts.Endpoints.BOOKING
import ru.dolmatovskaya.studio.consts.Endpoints.FINISH
import ru.dolmatovskaya.studio.consts.Endpoints.RESERVATION
import ru.dolmatovskaya.studio.reservation.booking.api.BookingApi
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingCreateRequestDto
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingListResponseDto
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingResponseDto
import ru.dolmatovskaya.studio.reservation.booking.service.BookingService
import ru.dolmatovskaya.studio.reservation.notification.mapper.NotificationMapper
import ru.dolmatovskaya.studio.reservation.notification.service.Notifier

private val logger = KotlinLogging.logger { }

@RestController
@RequestMapping("$API/$RESERVATION/$BOOKING")
@CrossOrigin("*")
@Validated
class BookingApiImpl(
    private val bookingService: BookingService,
    private val notifier: Notifier,
    private val notificationMapper: NotificationMapper
) : BookingApi {

    @PostMapping
    override fun create(@RequestBody dto: BookingCreateRequestDto): BookingResponseDto {
        logger.info { "Creation $dto" }
        val book = bookingService.book(dto)
        notifier.notifyAdmin(notificationMapper.fromBookingResponseDto(book))
        return book
    }

    @GetMapping
    override fun getBooking(
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) size: Int?,
        @RequestParam(required = false) ids: HashSet<Long>?,
        @RequestParam(required = false) phone: String?,
        @RequestParam(required = false) name: String?,
        @RequestParam(required = false) includeDone: Boolean?
    ): BookingListResponseDto {
        return bookingService.getBookings(page ?: 0, size ?: 25, ids, phone, name, includeDone)
    }

    @PostMapping("{id}/$APPROVE")
    override fun approve(@PathVariable id: Long) {
        bookingService.approve(id)
    }

    @PostMapping("{id}/$FINISH")
    override fun finish(@PathVariable id: Long) {
        bookingService.finish(id)
    }

    @DeleteMapping
    override fun delete(@RequestParam ids: Set<Long>) {
        bookingService.delete(ids)
    }
}