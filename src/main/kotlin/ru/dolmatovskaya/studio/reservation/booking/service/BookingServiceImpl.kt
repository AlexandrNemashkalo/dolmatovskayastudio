package ru.dolmatovskaya.studio.reservation.booking.service

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import ru.dolmatovskaya.studio.extensions.andLog
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingCreateRequestDto
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingListResponseDto
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingResponseDto
import ru.dolmatovskaya.studio.reservation.booking.mapper.BookingMapper
import ru.dolmatovskaya.studio.reservation.booking.model.BookingStatus
import ru.dolmatovskaya.studio.reservation.booking.repository.BookingRepository
import ru.dolmatovskaya.studio.reservation.stuff.service.StuffService

@Service
class BookingServiceImpl(
    private val bookingMapper: BookingMapper,
    private val bookingRepository: BookingRepository,
    private val stuffService: StuffService
) : BookingService {

    @Transactional
    override fun book(dto: BookingCreateRequestDto): BookingResponseDto {
        val alreadyBooked = stuffService.filterBookedIfAny(dto.stuffIds)
        if (alreadyBooked.isNotEmpty()) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Уже зарезервированы $alreadyBooked").andLog()
        }
        return bookingMapper.fromEntity(bookingRepository.save(bookingMapper.fromDto(dto)))
    }

    override fun getBookings(
        page: Int,
        size: Int,
        ids: Collection<Long>?,
        phone: String?,
        name: String?,
        includeDone: Boolean?
    ): BookingListResponseDto {
        val sort = Sort.by(Sort.Order.asc("id"))
        val result: List<BookingResponseDto> = if (size == -1) {
            val all = bookingRepository.findAll(sort)
            bookingMapper.fromEntities(all)
        } else if (!ids.isNullOrEmpty()) {
            val allByIds = bookingRepository.findAllById(ids)
            bookingMapper.fromEntities(allByIds)
        } else if (phone != null) {
            val allByPhone = bookingRepository.findAllByPhone(phone)
            bookingMapper.fromEntities(allByPhone)
        } else if (name != null) {
            val allByName = bookingRepository.findAllByName(name)
            bookingMapper.fromEntities(allByName)
        } else {
            val pageResult = bookingRepository.findAll(PageRequest.of(page, size, sort))
            bookingMapper.fromEntities(pageResult.content)
        }
        val ans = if (false == includeDone) {
            val tempRes = result.filter { it.status != BookingStatus.DONE }
            tempRes.size.toLong() to tempRes
        } else {
            result.size.toLong() to result
        }
        return BookingListResponseDto(ans.first, ans.second)
    }

    @Transactional
    override fun approve(id: Long) {
        val findById = bookingRepository.findById(id)
        findById.map {
            stuffService.book(it.stuffIds)
            bookingRepository.updateStatus(id, BookingStatus.IN_PROGRESS)
        }.orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND, "booking w/ id $id not found").andLog() }
    }

    @Transactional
    override fun finish(id: Long) {
        val findById = bookingRepository.findById(id)
        findById.map {
            stuffService.unbook(it.stuffIds)
            bookingRepository.updateStatus(id, BookingStatus.DONE)
        }.orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND, "booking w/ id $id not found").andLog() }
    }

    @Transactional
    override fun delete(ids: Collection<Long>) {
        val bookings = bookingRepository.findAllById(ids)
        stuffService.free(bookings.flatMap { it.stuffIds })
        bookingRepository.deleteAllByIdInBatch(ids)
    }
}