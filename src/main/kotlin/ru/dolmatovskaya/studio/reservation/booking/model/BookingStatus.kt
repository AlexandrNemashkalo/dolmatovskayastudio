package ru.dolmatovskaya.studio.reservation.booking.model

enum class BookingStatus {
    PRE_BOOKED,
    IN_PROGRESS,
    DONE
}
