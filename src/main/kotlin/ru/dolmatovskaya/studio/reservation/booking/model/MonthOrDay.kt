package ru.dolmatovskaya.studio.reservation.booking.model

enum class MonthOrDay {
    MONTHS, DAYS
}