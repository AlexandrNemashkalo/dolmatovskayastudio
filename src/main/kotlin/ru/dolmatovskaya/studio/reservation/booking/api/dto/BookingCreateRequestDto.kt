package ru.dolmatovskaya.studio.reservation.booking.api.dto

import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size
import ru.dolmatovskaya.studio.reservation.booking.model.DeliveryType
import ru.dolmatovskaya.studio.reservation.booking.model.MonthOrDay
import ru.dolmatovskaya.studio.reservation.booking.model.PaymentType

data class BookingCreateRequestDto(
    @field:NotBlank
    @field:Size(max = 64)
    val name: String,
    @field:NotBlank
    @field:Pattern(regexp = "\\+?\\d{11}")
    val phone: String,
    @field:Min(1)
    val quantity: Int,
    val monthOrDays: MonthOrDay,
    val deliveryType: DeliveryType,
    val paymentType: PaymentType,
    @field:NotEmpty
    val stuffIds: Set<Long> = emptySet()
)