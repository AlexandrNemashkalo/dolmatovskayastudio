package ru.dolmatovskaya.studio.reservation.booking.model

import java.time.LocalDateTime
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import ru.dolmatovskaya.studio.converter.LongCollectionConverter

@Entity
@Table(name = "booking")
@EntityListeners(AuditingEntityListener::class)
data class Booking(
    val name: String,
    val phone: String,
    @Enumerated(EnumType.STRING)
    val monthOrDay: MonthOrDay,
    val quantity: Int,
    @Enumerated(EnumType.STRING)
    val deliveryType: DeliveryType,
    @Enumerated(EnumType.STRING)
    val paymentType: PaymentType,
    @Enumerated(EnumType.STRING)
    val status: BookingStatus,
    @Convert(converter = LongCollectionConverter::class)
    val stuffIds: MutableSet<Long> = mutableSetOf(),
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "booking_id_seq_gen")
    @SequenceGenerator(name= "booking_id_seq_gen", sequenceName = "booking_id_seq", allocationSize = 1)
    val id: Long? = null,
    @field:CreatedDate
    val createdDateTime: LocalDateTime = LocalDateTime.now(),
    @field:LastModifiedDate
    val lastModifiedDateTime: LocalDateTime = LocalDateTime.now(),
)