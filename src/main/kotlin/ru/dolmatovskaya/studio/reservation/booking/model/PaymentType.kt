package ru.dolmatovskaya.studio.reservation.booking.model

enum class PaymentType {
    CASH, CARD
}