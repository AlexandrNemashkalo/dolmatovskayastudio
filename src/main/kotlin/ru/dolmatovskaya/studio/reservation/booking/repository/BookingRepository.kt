package ru.dolmatovskaya.studio.reservation.booking.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import ru.dolmatovskaya.studio.reservation.booking.model.Booking
import ru.dolmatovskaya.studio.reservation.booking.model.BookingStatus

interface BookingRepository : JpaRepository<Booking, Long> {
    fun findAllByPhone(phone: String): List<Booking>
    fun findAllByName(name: String): List<Booking>
    @Modifying
    @Query("update Booking b set b.status = :status where b.id = :id")
    fun updateStatus(@Param("id") id: Long, @Param("status") status: BookingStatus)
}