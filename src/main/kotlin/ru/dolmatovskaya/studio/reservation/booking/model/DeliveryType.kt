package ru.dolmatovskaya.studio.reservation.booking.model

enum class DeliveryType {
    SELF_PICKUP, COURIER
}