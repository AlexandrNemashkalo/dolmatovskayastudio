package ru.dolmatovskaya.studio.reservation.booking.api

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingCreateRequestDto
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingListResponseDto
import ru.dolmatovskaya.studio.reservation.booking.api.dto.BookingResponseDto

interface BookingApi {
    fun create(@Valid @RequestBody dto: BookingCreateRequestDto): BookingResponseDto
    fun getBooking(
        @PositiveOrZero
        @Parameter(required = false, `in` = ParameterIn.QUERY)
        page: Int?,
        @Parameter(required = false, `in` = ParameterIn.QUERY)
        @Min(-1)
        size: Int?,
        @Parameter(required = false, `in` = ParameterIn.QUERY)
        ids: HashSet<Long>? = null,
        @Parameter(required = false, `in` = ParameterIn.QUERY)
        phone: String? = null,
        @Parameter(required = false, `in` = ParameterIn.QUERY)
        name: String? = null,
        @Parameter(required = false, `in` = ParameterIn.QUERY)
        includeDone: Boolean? = false
    ): BookingListResponseDto
    fun approve(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long)
    fun finish(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long)
    fun delete(@NotEmpty ids: Set<@Positive Long>)
}