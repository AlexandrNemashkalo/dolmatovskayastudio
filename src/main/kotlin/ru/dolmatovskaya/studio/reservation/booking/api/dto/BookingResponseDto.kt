package ru.dolmatovskaya.studio.reservation.booking.api.dto

import java.time.LocalDateTime
import ru.dolmatovskaya.studio.reservation.booking.model.BookingStatus
import ru.dolmatovskaya.studio.reservation.booking.model.DeliveryType
import ru.dolmatovskaya.studio.reservation.booking.model.MonthOrDay
import ru.dolmatovskaya.studio.reservation.booking.model.PaymentType

data class BookingResponseDto (
    val name: String,
    val phone: String,
    val quantity: Int,
    val monthOrDay: MonthOrDay,
    val createdDateTime: LocalDateTime,
    val lastModifiedDateTime: LocalDateTime,
    val deliveryType: DeliveryType,
    val paymentType: PaymentType,
    val status: BookingStatus,
    val id: Long,
    val stuffIds: List<Long>
)