package ru.dolmatovskaya.studio.reservation.filter.api.dto

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive

data class FilterCreateDto(
    val parentId: Long? = null,
    @field:NotEmpty
    val label: String,
    @field:Positive
    val index: Int,
    val children: List<FilterCreateDto>? = null,
    @field:JsonProperty(access = JsonProperty.Access.READ_ONLY)
    val id: Long? = null
) {
    fun withId(id: Long) = FilterCreateDto(parentId, label, index, children, id)
}