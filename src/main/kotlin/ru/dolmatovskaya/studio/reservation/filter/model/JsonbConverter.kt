package ru.dolmatovskaya.studio.reservation.filter.model

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
open class JsonbConverter(
    private val objectMapper: ObjectMapper
) : AttributeConverter<FilterModel, String> {

    override fun convertToDatabaseColumn(attribute: FilterModel): String {
        return objectMapper.writeValueAsString(attribute)
    }

    override fun convertToEntityAttribute(dbData: String): FilterModel {
        return objectMapper.readValue(dbData)
    }
}