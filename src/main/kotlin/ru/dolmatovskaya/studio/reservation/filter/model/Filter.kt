package ru.dolmatovskaya.studio.reservation.filter.model

import ru.dolmatovskaya.studio.reservation.stuff.model.Sex

data class Filter(
    val sizes: List<String>?,
    val decades: List<String>?,
    val sex: List<Sex>?,
    val categories: List<String>?,
    val subCategories0: List<String>?,
    val subCategories: List<String>?,
)