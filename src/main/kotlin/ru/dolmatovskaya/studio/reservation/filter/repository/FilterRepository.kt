package ru.dolmatovskaya.studio.reservation.filter.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.dolmatovskaya.studio.reservation.filter.model.FilterEntity

interface FilterRepository : JpaRepository<FilterEntity, Int> {
    @Query("select nextval('public.filter_entity_id_seq')", nativeQuery = true)
    fun next(): Long
}