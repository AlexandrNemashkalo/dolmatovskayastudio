package ru.dolmatovskaya.studio.reservation.filter.service

import ru.dolmatovskaya.studio.reservation.filter.api.dto.FilterCreateDto
import ru.dolmatovskaya.studio.reservation.filter.api.dto.FilterListResponseDto
import ru.dolmatovskaya.studio.reservation.filter.model.Filter
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffListResponseDto

interface FilterService {
    fun getAllByFilter(page: Int, size: Int, filter: Filter): StuffListResponseDto
    fun create(dto: FilterCreateDto): FilterListResponseDto
    fun update(dto: FilterCreateDto): FilterListResponseDto
    fun get(): FilterListResponseDto
    fun delete(ids: Set<Long>)
}