package ru.dolmatovskaya.studio.reservation.filter.mapper

import org.mapstruct.Mapper
import ru.dolmatovskaya.studio.reservation.filter.model.Filter
import ru.dolmatovskaya.studio.reservation.stuff.model.Sex

@Mapper
interface FilterMapper {
    fun fromQueries(
        sizes: List<String>?,
        decades: List<String>?,
        sex: List<Sex>?,
        categories: List<String>?,
        subCategories0: List<String>?,
        subCategories: List<String>?
    ): Filter = Filter(sizes?.map { it.uppercase() }, decades?.map { it.uppercase() }, sex, categories?.map { it.uppercase() }, subCategories0?.map { it.uppercase() }, subCategories?.map { it.uppercase() })
}