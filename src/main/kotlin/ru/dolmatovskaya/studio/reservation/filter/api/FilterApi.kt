package ru.dolmatovskaya.studio.reservation.filter.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.validation.Valid
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Positive
import org.springframework.validation.annotation.Validated
import ru.dolmatovskaya.studio.reservation.filter.api.dto.FilterCreateDto
import ru.dolmatovskaya.studio.reservation.filter.api.dto.FilterListResponseDto

@Validated
interface FilterApi {
    @Operation(description = "Создать фильтр", summary = "Для авторизованных", responses = [ApiResponse(description = "created", responseCode = "201")])
    fun create(@Valid @RequestBody dto: FilterCreateDto): FilterListResponseDto

    @Operation(description = "Обновить фильтр", summary = "Для авторизованных", responses = [ApiResponse(description = "OK", responseCode = "200")])
    fun update(@Positive @Parameter(`in` = ParameterIn.PATH) id: Long, @Valid @RequestBody dto: FilterCreateDto): FilterListResponseDto

    @Operation(description = "Получить все фильтры", summary = "Для неавторизованных", responses = [ApiResponse(description = "OK", responseCode = "200")])
    fun get(): FilterListResponseDto

    @Operation(description = "Удалить фильтр", summary = "Для авторизованных", responses = [ApiResponse(description = "OK", responseCode = "200")])
    fun delete(@Parameter(`in` = ParameterIn.QUERY) @NotEmpty ids: Set<Long>)
}