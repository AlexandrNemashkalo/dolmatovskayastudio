package ru.dolmatovskaya.studio.reservation.filter.model

import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef

@Table(name = "filters")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType::class)
data class FilterEntity(
//    @Convert(converter = JsonbConverter::class)
    @Type(type = "jsonb")
    val value: FilterModel,
    @Id
    val id: Int = 1
)