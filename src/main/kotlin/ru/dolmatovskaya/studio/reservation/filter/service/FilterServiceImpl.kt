package ru.dolmatovskaya.studio.reservation.filter.service

import javax.persistence.criteria.Predicate
import mu.KotlinLogging
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import ru.dolmatovskaya.studio.extensions.orElseThrowNotFound
import ru.dolmatovskaya.studio.reservation.filter.api.dto.FilterCreateDto
import ru.dolmatovskaya.studio.reservation.filter.api.dto.FilterListResponseDto
import ru.dolmatovskaya.studio.reservation.filter.api.dto.FilterResponseDto
import ru.dolmatovskaya.studio.reservation.filter.model.Filter
import ru.dolmatovskaya.studio.reservation.filter.model.FilterModel
import ru.dolmatovskaya.studio.reservation.filter.repository.FilterRepository
import ru.dolmatovskaya.studio.reservation.stuff.api.dto.StuffListResponseDto
import ru.dolmatovskaya.studio.reservation.stuff.mapper.StuffMapper
import ru.dolmatovskaya.studio.reservation.stuff.model.Sex
import ru.dolmatovskaya.studio.reservation.stuff.model.Stuff
import ru.dolmatovskaya.studio.reservation.stuff.repository.StuffRepository

private val logger = KotlinLogging.logger { }

@Service
class FilterServiceImpl(
    private val stuffRepository: StuffRepository,
    private val stuffMapper: StuffMapper,
    private val filterRepository: FilterRepository
) : FilterService {

    private fun <T> List<T>?.applyIfNotNullAndNotEmpty(f: (List<T>) -> Unit) {
        if (this != null && this.isNotEmpty()) { f(this) }
    }

    override fun getAllByFilter(page: Int, size: Int, filter: Filter): StuffListResponseDto {
        val where = Specification.where<Stuff?> { root, _, criteriaBuilder ->
            val acc: MutableList<Predicate?> = mutableListOf()

            filter.sex.applyIfNotNullAndNotEmpty { acc += criteriaBuilder.`in`(root.get<List<Sex>>("sex")).value(it) }
            filter.sizes.applyIfNotNullAndNotEmpty { acc += criteriaBuilder.`in`(root.get<List<String>>("size")).value(it) }
            filter.decades.applyIfNotNullAndNotEmpty { acc += criteriaBuilder.`in`(root.get<List<String>>("decades")).value(it) }
            filter.categories.applyIfNotNullAndNotEmpty { acc += criteriaBuilder.`in`(root.get<List<String>>("category")).value(it) }
            filter.subCategories0.applyIfNotNullAndNotEmpty { acc += criteriaBuilder.`in`(root.get<List<String>>("subCategory0")).value(it) }
            filter.subCategories.applyIfNotNullAndNotEmpty { acc += criteriaBuilder.`in`(root.get<List<String>>("subCategory")).value(it) }

            val target = acc.filterNotNull()
            criteriaBuilder.and(*target.toTypedArray())
        }

        logger.info { "filter = $filter" }
        val entities = stuffRepository.findAll(where, PageRequest.of(page, size, Sort.by(Sort.Order.asc("id"))))
        val result = stuffMapper.fromEntities(entities.content)
        return StuffListResponseDto(entities.totalElements, result)
    }

    override fun create(dto: FilterCreateDto): FilterListResponseDto {
        logger.info { "Creating $dto" }
        val entity = filterRepository.findById(1).orElseThrowNotFound("filter not found")
        val model = entity.value

        if (dto.parentId != null) {
            logger.info { "paretn id != null for $dto" }
            val findDeep = findDeep(dto.parentId, model.children)
            if (findDeep?.children == null) {
                findDeep?.children = mutableListOf()
            }
            findDeep?.children?.add(map(listOf(dto))!!.first())
        } else {
            model.children?.add(map(listOf(dto))!!.first())
        }
        filterRepository.save(entity)
        val result = mapReverse(model.children!!)
        return FilterListResponseDto(result.size.toLong(), result)
    }

    override fun update(dto: FilterCreateDto): FilterListResponseDto {
        logger.info { "updating $dto" }
        val model = filterRepository.findById(1).orElseThrowNotFound("filter not found")
        if (dto.parentId != null) {
            logger.info { "parent id != null for $dto" }
            val parent = findDeep(dto.parentId, model.value.children)
            if (parent != null) {
                val children = findDeep(dto.id!!, model.value.children)?.children
                deleteRec(setOf(dto.id), model.value.children)
                val target = map(listOf(dto))!!.first()
                target.children = children
                if (parent.children == null) {
                    parent.children = mutableListOf(target)
                } else {
                    parent.children?.add(target)
                }
            }
        }
        findDeep(dto.id!!, model.value.children)?.apply { index = dto.index; label = dto.label }
        filterRepository.save(model)
        val result = mapReverse(model.value.children!!)
        return FilterListResponseDto(result.size.toLong(), result)
    }

    override fun get(): FilterListResponseDto {
        val filterModel = filterRepository.findById(1).orElseThrowNotFound("filter not found").value
        return FilterListResponseDto(filterModel.children!!.size.toLong(), mapReverse(filterModel.children!!))
    }

    override fun delete(ids: Set<Long>) {
        logger.info { "deleting $ids" }
        val filterModel = filterRepository.findById(1).orElseThrowNotFound("filter not found")
        deleteRec(ids, filterModel.value.children!!)
        filterRepository.save(filterModel)
    }

    private fun deleteRec(ids: Set<Long>, children: MutableList<FilterModel>?) {
        if (children == null) {
            return
        }
        children.removeIf { it.id in ids }
        for (c in children) {
            deleteRec(ids, c.children)
        }
    }

    fun findDeep(id: Long, children: List<FilterModel>?): FilterModel? {
        val r = children?.find { id == it.id }
        if (r == null) {
            for (c in children ?: emptyList()) {
                val f = findDeep(id, c.children)
                if (f != null) {
                    return f
                }
            }
        } else {
            return r
        }
        return null
    }

    fun map(dtos: List<FilterCreateDto>?): MutableList<FilterModel>? =
        dtos?.map { FilterModel(filterRepository.next(), it.label, it.index, map(it.children)) }
            ?.toMutableList()

    fun mapReverse(source: List<FilterModel>) : List<FilterResponseDto> =
        source.map { FilterResponseDto(it.id!!, it.label, it.index, it.children?.let { mapReverse(it) })}.sortedBy { it.index }

}