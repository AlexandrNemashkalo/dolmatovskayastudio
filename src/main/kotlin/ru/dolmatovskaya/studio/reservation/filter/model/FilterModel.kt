package ru.dolmatovskaya.studio.reservation.filter.model

data class FilterModel(
    val id: Long? = null,
    var label: String,
    var index: Int,
    var children: MutableList<FilterModel>? = null
)