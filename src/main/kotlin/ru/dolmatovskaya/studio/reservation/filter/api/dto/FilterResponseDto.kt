package ru.dolmatovskaya.studio.reservation.filter.api.dto

data class FilterResponseDto(
    val id: Long,
    val label: String,
    val index: Int,
    val children: List<FilterResponseDto>? = null
)