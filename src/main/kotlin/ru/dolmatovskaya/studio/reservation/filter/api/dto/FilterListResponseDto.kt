package ru.dolmatovskaya.studio.reservation.filter.api.dto

data class FilterListResponseDto(
    val totalCount: Long = 1,
    val value: List<FilterResponseDto>
)