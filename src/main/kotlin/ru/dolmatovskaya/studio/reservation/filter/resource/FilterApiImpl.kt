package ru.dolmatovskaya.studio.reservation.filter.resource

import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import ru.dolmatovskaya.studio.consts.Endpoints.API
import ru.dolmatovskaya.studio.consts.Endpoints.FILTERS
import ru.dolmatovskaya.studio.reservation.filter.api.FilterApi
import ru.dolmatovskaya.studio.reservation.filter.api.dto.FilterCreateDto
import ru.dolmatovskaya.studio.reservation.filter.api.dto.FilterListResponseDto
import ru.dolmatovskaya.studio.reservation.filter.service.FilterService

@RequestMapping("$API/$FILTERS")
@RestController
@CrossOrigin("*")
@Validated
class FilterApiImpl(
    private val filterService: FilterService
) : FilterApi {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    override fun create(@RequestBody dto: FilterCreateDto): FilterListResponseDto {
        return filterService.create(dto)
    }

    @PutMapping("{id}")
    override fun update(@PathVariable id: Long, @RequestBody dto: FilterCreateDto): FilterListResponseDto {
        return filterService.update(dto.withId(id))
    }

    @GetMapping
    override fun get(): FilterListResponseDto {
        return filterService.get()
    }

    @DeleteMapping
    override fun delete(@RequestParam ids: Set<Long>) {
        filterService.delete(ids)
    }
}