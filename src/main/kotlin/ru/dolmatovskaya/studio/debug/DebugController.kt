package ru.dolmatovskaya.studio.debug

import org.springframework.context.annotation.Profile
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.dolmatovskaya.studio.reservation.stuff.repository.StuffRepository

@RestController
@RequestMapping("api/debug")
@Profile("staging")
class DebugController(
    private val stuffRepository: StuffRepository
) {
    @DeleteMapping("stuffs")
    fun deleteAllStuff() {
        stuffRepository.deleteAllInBatch()
    }
}