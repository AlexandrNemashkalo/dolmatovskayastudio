import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.6.1"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.6.0"
	kotlin("plugin.spring") version "1.6.0"
	kotlin("plugin.jpa") version "1.6.0"
	kotlin("kapt") version "1.6.0"
}

group = "ru.dolmatovskaya.studio"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	kapt("org.mapstruct:mapstruct-processor:1.4.2.Final")
	implementation("org.mapstruct:mapstruct-jdk8:1.4.2.Final")
	implementation("org.mapstruct:mapstruct:1.4.2.Final")

	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.13.0")
	implementation("org.springframework.boot:spring-boot-starter-validation")

	implementation("org.springdoc:springdoc-openapi-ui:1.5.13")
	implementation("org.springdoc:springdoc-openapi-security:1.5.13")
	implementation("org.springdoc:springdoc-openapi-kotlin:1.5.13")

	implementation("io.jsonwebtoken:jjwt:0.9.1")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("io.github.microutils:kotlin-logging-jvm:2.0.10")
	implementation("org.liquibase:liquibase-core")
	implementation("com.vladmihalcea:hibernate-types-52:2.16.2")
	runtimeOnly("org.postgresql:postgresql")

	testImplementation("io.mockk:mockk:1.12.4")
	testImplementation("org.testcontainers:postgresql:1.16.2")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=all")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

kapt {
	arguments {
		arg("mapstruct.suppressGeneratorTimestamp", "true")
		arg("mapstruct.suppressGeneratorVersionInfoComment", "true")
		arg("mapstruct.defaultComponentModel", "spring")
		arg("mapstruct.defaultInjectionStrategy", "constructor")
	}
}